<?php

return [
	'mode'               => '',
    'format'             => 'A3-L',
    'defaultFontSize'    => '14',
    'defaultFont'        => 'sans-serif',
    'marginLeft'         => 5,
    'marginRight'        => 5,
    'marginTop'          => 5,
    'marginBottom'       => 10,
    'marginHeader'       => 0,
    'marginFooter'       => 0,
    'orientation'        => 'P',
    'title'              => 'Laravel PDF',
    'author'             => '',
    'watermark'          => '',
    'showWatermark'      => false,
    // 'watermarkFont'      => 'sans-serif',
    'SetDisplayMode'     => 'fullpage',
	'watermarkTextAlpha' => 0.1,
	'font_path' => base_path('/resources/fonts/'), // don't forget the trailing slash!

	'font_data' => [
		"khmerosmuol" => [/* Khmer */
			'R' => "khmerosmuol.ttf",
			// 'useOTL' => 0xFF,
		],
		"khmerosmuollight" => [/* Khmer */
			'R' => "khmerosmuollight.ttf",
			'useOTL' => 0xFF,
		],
		"khmerosbokor" => [/* Khmer */
			'R' => "khmerosbokor.ttf",
			'useOTL' => 0xFF,
		],
		"khmerosmuolpali" => [/* Khmer */
			'R' => "khmerosmuolpali.ttf",
			// 'useOTL' => 0xFF,
			// 'useKashida' => 75
		]
    // ...add as many as you want.
	]
	// 'mode' => 'utf-8',
	// 'format' => 'A4',
	// 'author' => 'Sotheng',
	// 'subject' => '',
	// 'keywords' => '',
	// 'creator' => 'Laravel Pdf',
	// 'display_mode' => 'fullpage',
	// // 'tempDir' => base_path('../temp/'),
	// 'font_path' => base_path('resources/fonts/'),

	
];
