<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
// Route::resource('shares', 'ShareController');


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home'); 

    Route::resource('ProfessorClasses', 'V2\ProfessorClassController');
    
    Route::group([
        'prefix' => 'head_study',
        'middleware' => ['auth.admin', 'auth.head_study']
    ], function () {
           
        Route::resource('buildings', 'V2\BuildingController');
        Route::resource('floors', 'V2\FloorController', ['except' => ['create']]);
        Route::get('building/{building_id}/floors/create', 'V2\FloorController@create')->name('floors.create');
        Route::resource('rooms', 'V2\RoomController', ['except' => ['create']]);
        Route::get('floor/{floor_id}/rooms/create', 'V2\RoomController@create')->name('rooms.create');

        Route::get('/buildings/{id}/schedule', 'V2\BuildingScheduleController@index')->name('building.schedule');
        Route::get('/buildings/{id}/schedule/test', 'V2\BuildingScheduleController@test')->name('building.schedule.test');
        Route::get('/buildings/schedule/{time}', 'V2\BuildingScheduleController@filterClasses');
        Route::get('/buildings/schedule/fetchDay', 'V2\BuildingScheduleController@filterDay')->name('day.fetch');
        Route::post('/buildings/schedule/{id}', 'V2\BuildingScheduleController@store')->name('BuildingStoreAssignedClass');

        Route::get('/buildings/{building_id}/room/{room_id}', 'V2\RoomScheduleController@index')->name('ShowRoomSchedule');
        Route::get('/buildings/{building_id}/room/{room_id}/test', 'V2\RoomScheduleController@test');
        Route::get('/buildings/room/schedule/{time}', 'V2\RoomScheduleController@filterClasses');
        Route::post('/buildings/{building_id}/room/schedule/{room_id}', 'V2\RoomScheduleController@store')->name('RoomStoreAssignedClass');
        Route::get('/roomDynamicGroupDependent/fetch', 'V2\RoomScheduleController@filteredDepartmentGroup')->name('roomDynamicGroup.fetch');

        Route::get('/dynamicDepartmentDependent/fetch', 'V2\BuildingScheduleController@filteredFacultyDepartment')->name('dynamicDepartment.fetch');
        Route::get('/dynamicGroupDependent/fetch', 'V2\BuildingScheduleController@filteredDepartmentGroup')->name('dynamicGroup.fetch');
        Route::get('/dynamicSubgroupDependent/fetch', 'V2\BuildingScheduleController@filteredGroupSubgroup')->name('dynamicSubgroup.fetch');
        Route::get('/used_room/filterAvailableRooms', 'V2\UsedRoomController@filterAvailableRooms')->name('used_rooms.index');

        Route::get('/{semester}/{department_id}/{group_id}/download/schedules','V2\DownloadScheduleController@donwloadPdf')->name('download.schedule');
        Route::get('/{building_id}/print/schedules','V2\DownloadScheduleController@index')->name('print.schedule');
        Route::get('/print/schedules','V2\DownloadScheduleController@testing')->name('testing.schedule');

    
        // classes
        Route::get('/index', 'V2\ClassController@index')->name("ShowAllCreateClass");
        Route::get('/create', 'V2\ClassController@create')->name("ClassesCreate");
        Route::post('/store', 'V2\ClassController@store')->name("StoreClassesCreate");
        Route::get('/{id}/edit', 'V2\ClassController@edit')->name("EditClassesCreate");
        Route::post('/update/{id}','V2\ClassController@update')->name("UpdateClassCreate");
        Route::post('/delete/{id}','V2\ClassController@destroy')->name("DeleteClassesCreate");  

        /**
         * add room to use in building 
         * */    
        Route::put('/buildings/{building_id}/add_used_room/{room_id}', 'V2\UsedRoomController@update')->name('AddUseRoom');
        /**
         * add room to use in used room tab
         */
        Route::put('/used_room/addRoomTab', 'V2\UsedRoomController@updateRoom')->name('AddUseRoomTab');

        Route::put('/buildings/{building_id}/delete_used_room/{room_id}', 'V2\UsedRoomController@delete')->name('used_rooms.delete');
        Route::get('/used_room', 'V2\UsedRoomController@index')->name('ShowAllUsedRoom');

        Route::group([
            'prefix' => 'admin',
            'middleware' => ['auth.admin']
        ], function () {
            Route::resource('faculties', 'V2\FacultyController');
            Route::resource('departments', 'V2\DepartmentController');
            Route::resource('subjects', 'V2\SubjectController');
            Route::resource('professors', 'V2\ProfessorController');
            Route::resource('rooms', 'V2\RoomController');
        });
    }); 
});

// Route::group([
//     'prefix' => 'v2',
//     'namespace' => 'V2',
//     'middleware' => ['auth.priority']
// ], function() {

//     Route::get('/home', 'HomeController@index')->name('home');

    // Route::group([
    //     'prefix' => 'classes'
    // ], function () {
    //     Route::get('/', 'ClassController@index')->name("ShowAllCreateClass");
    //     Route::get('/create', 'ClassController@create')->name("ClassesCreate");
    //     Route::post('/store', 'ClassController@store')->name("StoreClassesCreate");
    //     Route::get('/{id}/edit', 'ClassController@edit')->name("EditClassesCreate");
    //     Route::post('/update/{id}','ClassController@update')->name("UpdateClassCreate");
    //     Route::post('/delete/{id}','ClassController@destroy')->name("DeleteClassesCreate");
    
    // });

    // Route::group([
    //     'prefix' => 'controls'
    // ], function () {
    //     //Building
    //     Route::resource('buildings', 'BuildingController');
    //     //Floor
    //     Route::resource('floors', 'FloorController', ['except' => ['create']]);
    //     Route::get('building/{building_id}/floors/create', 'FloorController@create')->name('floors.create');
    //     //Room
    //     Route::resource('rooms', 'RoomController', ['except' => ['create']]);
    //     Route::get('floor/{floor_id}/rooms/create', 'RoomController@create')->name('rooms.create');
    // });

    // Route::group([
    //     'prefix' => 'building-schedules'
    // ], function () {
    //     // schedule for each building 
    //     Route::get('/buildings/{id}/schedule', 'BuildingScheduleController@index')->name('building.schedule');
    //     Route::get('/buildings/{id}/schedule/test', 'BuildingScheduleController@test')->name('building.schedule.test');
    //     Route::get('/buildings/schedule/{time}', 'BuildingScheduleController@filterClasses');
    //     Route::get('/buildings/schedule/fetchDay', 'BuildingScheduleController@filterDay')->name('day.fetch');
    //     Route::post('/buildings/schedule/{id}', 'BuildingScheduleController@store')->name('BuildingStoreAssignedClass');
    // });

    // Route::group([
    //     'prefix' => 'room-schedules'
    // ], function () {
    //     // schedule for each room
    //     Route::get('/buildings/{building_id}/room/{room_id}', 'RoomScheduleController@index')->name('ShowRoomSchedule');
    //     Route::get('/buildings/{building_id}/room/{room_id}/test', 'RoomScheduleController@test');
    //     Route::get('/buildings/room/schedule/{time}', 'RoomScheduleController@filterClasses');
    //     Route::post('/buildings/{building_id}/room/schedule/{room_id}', 'RoomScheduleController@store')->name('RoomStoreAssignedClass');
    //     Route::get('/roomDynamicGroupDependent/fetch', 'RoomScheduleController@filteredDepartmentGroup')->name('roomDynamicGroup.fetch');
    // });

    // Route::group([
    //     'prefix' => 'ajax-fetchs'
    // ], function () {
        
    //     // filter the same group in the department
    //     Route::get('/dynamicDepartmentDependent/fetch', 'BuildingScheduleController@filteredFacultyDepartment')->name('dynamicDepartment.fetch');
    //     Route::get('/dynamicGroupDependent/fetch', 'BuildingScheduleController@filteredDepartmentGroup')->name('dynamicGroup.fetch');
    //     Route::get('/dynamicSubgroupDependent/fetch', 'BuildingScheduleController@filteredGroupSubgroup')->name('dynamicSubgroup.fetch');
    //     Route::get('/used_room/filterAvailableRooms', 'UsedRoomController@filterAvailableRooms')->name('used_rooms.index');
    // });

    // Route::group([
    //     'prefix' => 'download-schedules'
    // ], function () {
  
    //     // Route for download schedule
    //     Route::get('/{semester}/{department_id}/{group_id}/download/schedules','DownloadScheduleController@donwloadPdf')->name('download.schedule');
    //     Route::get('/{building_id}/print/schedules','DownloadScheduleController@index')->name('print.schedule');
    //     Route::get('/print/schedules','DownloadScheduleController@testing')->name('testing.schedule');
    // });

    // Route::group([
    //     'prefix' => 'head-deparment',
    //     'middleware' => ['auth.head_department']
    // ], function () {

    //     // for Classes route
    //     Route::get('/classes', 'ClassController@index')->name("ShowAllCreateClass");
    //     Route::get('/classes/create', 'ClassController@create')->name("ClassesCreate");
    //     Route::post('/classes/store', 'ClassController@store')->name("StoreClassesCreate");
    //     Route::get('/classes/{id}/edit', 'ClassController@edit')->name("EditClassesCreate");
    //     Route::post('/classes/update/{id}','ClassController@update')->name("UpdateClassCreate");
    //     Route::post('/classes/delete/{id}','ClassController@destroy')->name("DeleteClassesCreate");

    //     Route::resource('subjects', 'SubjectController');
    // });

    // Route::group([
    //     'prefix' => 'use-room'
    // ], function () {
    //     // used room route
    //     /**
    //      * add room to use in building 
    //      * */    
    //     Route::put('/buildings/{building_id}/add_used_room/{room_id}', 'UsedRoomController@update')->name('AddUseRoom');
    //     /**
    //      * add room to use in used room tab
    //      */
    //     Route::put('/used_room/addRoomTab', 'UsedRoomController@updateRoom')->name('AddUseRoomTab');

    //     Route::put('/buildings/{building_id}/delete_used_room/{room_id}', 'UsedRoomController@delete')->name('used_rooms.delete');
    //     Route::get('/used_room', 'UsedRoomController@index')->name('ShowAllUsedRoom');
    // });

    // Route::group([
    //     'prefix' => 'foundation-year',
    //     'middleware' => ['auth.foundation_year']
    // ], function () {
        
    // });

    // Route::group([
    //     'prefix' => 'study-office',
    //     'middleware' => ['auth.study_office']
    // ], function () {
        
    // });

    // Route::group([
    //     'prefix' => 'admin',
    //     'middleware' => ['auth.admin']
    // ], function () {
    //     // new user
    //     // new building
    //     // new room
    //     // new faculty
    //     // new department

    // });

// });


// Route::get('/home', 'HomeController@index')->name('home');
