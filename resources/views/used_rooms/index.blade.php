@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <ol class="breadcrumb">
        <h2 class="breadcrumb-item">Used Room list:</h2>
    </ol>
    <div class="card-body">    
        @foreach ($buildings as $building)
            <a class="pull-right m-2" href="#" data-building_id="{{$building->id}}" data-toggle="modal" data-target="#useRoomModal" id="myButton">Use New Room <i class="fa fa-plus-square fa-lg"></i></a>
            <div class="card-header">
                <h4>{{$building->name}}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive-sm">
                    <div class="box">
                        <div class="container-fluid">
                            <table class="table">
                                    <thead>
                                        <th>Room name</th>
                                        <th>Total Students</th>
                                        <th>Action</th>
                                    </thead>
                                @foreach ($rooms as $room)
                                @if ($room->building_id === $building->id)
                                    <tbody>
                                        <th>{{ $room->name }}</th>
                                        <td>{{$room->total_students}}</td>
                                        <td>
                                            {!! Form::open(['route' => ['used_rooms.delete', $building->id,$room->id], 'method' => 'put']) !!}
                                                {!! Form::button('<i class="fa fa-trash"  title="Delete Room"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'style' => 'height: 40px;color: red;', 'onclick' => "return confirm('Are you sure Unuse $room->name?')"]) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tbody>
                                    
                                @endif
                            @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div> 


    <!-- Modal -->
    <div id="useRoomModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Use New Room</h4>
            </div>
            <form id="addUsedRoom_form" method="put">
                {{method_field('PUT')}}
                {{csrf_field()}}
                <div class="modal-body">
                        <label> Available Rooms:</label><br />
                        <select id="filteredAvailableRooms" name="room_id">
                            <option value="">Choose Rooms</option>
                        </select>
                        <input name="building_id" hidden="true" type="number"/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>

        $(document).ready(function() {
            $('#useRoomModal').on('show.bs.modal', function (event) {

                var aTag = $(event.relatedTarget)
                var building_id = aTag.data('building_id');
                console.log(building_id)

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('used_rooms.index')}}",
                    method: "GET",
                    data: ({
                        building_id: building_id,
                    }),
                    success: function (filteredResult) {
                        console.log("Result: "+filteredResult);
                        $('#filteredAvailableRooms').html(filteredResult);
                    }
                })

                $('select').on('change', function() {
                    var room_id =  this.value ;
                    $('input[name="building_id"]').val(building_id);
                    console.log(room_id);
                    $('form#addUsedRoom_form').attr('action', "{{ route('AddUseRoomTab')}}");

                });

            })
        });

    </script>
 
@endsection
