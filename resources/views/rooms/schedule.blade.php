@extends('layouts.app')

@section('content')
    <div style="margin: 10px 0px 0px 20px;">
        <h2>Schedule Room: {{$room->name}}</h2>
        <?php $room_name = $room->name ?>
        <?php $room_detail = $room ?>
        <div style="display: flex;align-items: center;justify-content: center">
            <label><u>S</u>elect Schedules: </label>
            <select name="select_show_room" style="margin-left: 10px;" onchange="top.location.href = this.options[this.selectedIndex].value">
                    <option value="">Schedule options</option>
                    <option value="{{ route("building.schedule", ["id" => $building_id]) }}">Building Schedule</option>
                @foreach ($rooms as $room)
                    {{-- @if ($room->building_id === $building_id) --}}
                        <option value="{{ route("ShowRoomSchedule", ["building_id" => $building_id, "room_id" => $room]) }}">{{$room->name}}</option>
                    {{-- @endif --}}
                @endforeach
            </select>
        </div>
        <br>
    </div>
    <div id="container" style="padding: 0px 15px 0px 15px;">
        <table id="customers" class="table table-bordered" style="text-align: center;">
            <thead>
                <tr>
                    <th style="width: 120px;">Time & Day</th>
                    @foreach($days as $day)
                        <th>{{$day->name}}</th>
                    @endforeach
                </tr>
            </thead>

            <tbody>
                @php
                    $is =0;
                    $i = 0;
                    $row_val = 0;
                    $col_val = 0;
                    $array_spans[32][6]= -1 ;
                    $array_compare[32][7] = -1;
                    for ($row = 0; $row < 32; $row++)
                    {
                        for ($col = 0; $col < 6; $col++)
                        {
                            $array_spans[$row][$col] = -1;
                        }
                    }
                    for ($row = 0; $row < 32; $row++)
                    {
                        for ($col = 0; $col < 7; $col++)
                        {
                            $array_compare[$row][$col] = -1;
                        }
                    }
                @endphp

                @foreach($times as $time)
                <?php 
                    $assigned_end = false; 
                    $assigned_day = false;
                    $col_val = 0;
                ?>
                    <tr>
                        @if ($i%2 ===0)
                            <th rowspan="2">{{$time->time}}</th>
                        @endif
                        @foreach($days as $day)
                            @php
                                $assigned_flag = false;
                            @endphp
                            @foreach ($assigned_rooms as $assigned_room)
                                @if ($assigned_room->start_time === $time->time && $assigned_room->day->name === $day->name && $assigned_room->room->name === $room_name)
                                    <td rowspan="{{$assigned_room->row_span_num}}" style="background-color:#dc3545;padding: 0px;height: 51px; width: 221px;">
                                        <button type="button" data-time="{{$time->time}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->room_name}}"  class="btn btn-light  btn-xs col-lg-12" style=" width: 221px;white-space: normal;font-size: 12px;border-radius:0px;background-color:#dc3545; border-color:#dc3545; color:white;" data-toggle="modal" data-target="#myModal" id="myButton" disabled="true" >
                                            {{$assigned_room->class->subject->name}}
                                            @php 
                                                $assigned_end = $assigned_room->end_time; 
                                                $assigned_day = $assigned_room->day->name;
                                                $array_spans[$row_val][$col_val] = $assigned_room->row_span_num - 1;
                                                $assigned_flag = true;
                                                
                                            @endphp
                                            @for($j = 0; $j< $assigned_room->row_span_num; $j++)
                                                @php
                                                    $tmp = $row_val;
                                                    $array_compare[$tmp+$j][$col_val] = $assigned_room->row_span_num-$j;
                                                @endphp
                                            @endfor
                                        </button>
                                    </td>
                                @endif
                            @endforeach
                            @if ($row_val < 33 )
                                @if ($assigned_flag === false && $array_compare[$row_val][$col_val] === -1)
                                    <td style="padding: 0px;height: 51px; width: 221px;">
                                        <button type="button" data-room_seats={{$room->total_students}} data-time="{{$time->time}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->room_name}}"  class="btn btn-light" style="height: 51px; width: 221px;white-space: normal; font-size: 12px;border-radius:0px;" data-toggle="modal" data-target="#myModal" id="myButton">
                                        </button>
                                    </td>
                                @endif
                            @endif

                            @php
                                $col_val++;
                            @endphp
                        @endforeach
                    </tr>
                    @php
                        $i++;
                        $row_val++;
                    @endphp
                @endforeach
            </tbody>
        </table>
    </div>

    {{-- Modal --}}
    <div id="myModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="text-align:center;">
                    <div class="modal-header" style="background-color:#3700B3; color:white;">
                        <h5 class="modal-title" id="exampleModalLabel" style="margin: 0 auto;">ASSIGN CLASSROOM DIALOG</h5>
                    </div>
                    <div class="modal-body" style="padding: 50px 0px 50px 80px;">
                        <form action="{{route('RoomStoreAssignedClass', ['building' => $building_id, 'room_id' => $room_detail->id ])}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                                <label class="col-sm-2 col-form-label">Department </label>
                                <div class="col-sm-10">
                                    <select type="text" class="form-control dynamic" id="department_name" name="department_id" data-department="group">
                                        <option value="">Select Department</option>
                                        <!-- to display data after group by method -->
                                        @foreach ($departments as $department )
                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                                <label class="col-sm-2 col-form-label">Groups Type </label>
                                <div class="col-sm-10">
                                    <select type="text" class="form-control dynamic" id="group" name="group_id" data-department="group_name">
                                        <option value="">Select Groups Type</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                                <label class="col-sm-2 col-form-label">Class </label>
                                <div class="col-sm-10">
                                    <select type="text" class="form-control" id="class_id" name="class_id" placeholder="Class Name">
                                        <option id="class_init_opt" value="">Select Subject</option>
                                        {{-- @foreach ($classes as $class) --}}
                                            {{-- @if ($class->time == time) --}}
                                                {{-- <option value="{{$class->id}}">{{$class->subject->subject_name}}</option> --}}
                                            {{-- @endif --}}
                                        {{-- @endforeach --}}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                                <label class="col-sm-2 col-form-label">Day </label>
                                <div class="col-sm-10">
                                    <select type="text" class="form-control" id="day_id" name="day_id" placeholder="Class Name">
                                        <option id="day_init_opt" value="">Teaching Day</option>
                                        @foreach ($days as $day)
                                            <option value="{{$day->id}}">{{$day->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                                <label class="col-sm-2 col-form-label">Start </label>
                                <div class="col-sm-10">
                                    <select type="text" class="form-control" id="start_time" name="start_time" placeholder="Class Name">
                                            <option id="start_init_opt" value="">Start Time</option>
                                        @foreach ($times as $time)
                                            <option value="{{$time->time}}">{{$time->time}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                                <label class="col-sm-2 col-form-label">End </label>
                                <div class="col-sm-10">
                                    <select type="text" class="form-control" id="end_time" name="end_time" placeholder="End Time">
                                        <option id="end_init_opt" value="">End Time</option>
                                        @foreach ($times as $time)
                                            <option value="{{$time->time}}">{{$time->time}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{-- hidden input --}}
                            <input type="hidden" class="room_id" name="room_id" id="room_id" value="null">

                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                            <button type="submit" id="save_assign" class="btn btn-primary" style="margin-left: 20px;background-color: #7db300;border-color:#7db300;">Save Assign</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>
        $(document).ready(function(){

            var getSelectedTime;
            // function to get the event of click and send to modal
            $('#myModal').on('show.bs.modal', function (event) {

                var button = $(event.relatedTarget)
                var day = button.data('day');
                var room_id = button.data('room_id');
                var day_id = button.data('day_id');
                var time = button.data('time');
                console.log(time)

                var modal =$(this); // get the this modal DOM

                modal.find('form #day_init_opt').val(day_id)
                modal.find('form #day_init_opt').text(day)
                modal.find('form #room_id').val(room_id)
                modal.find('form if').val(time)

           })

           // dependent selection from from department to group
            $('#department_name').change(function () {
                if($(this).val() != ''){
                    var select = $(this).attr( 'id');
                    var value = $(this).val();
                    var dependent = $(this).data('department');
                    console.log(select);
                    console.log(value);
                    console.log(dependent);

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{ route('roomDynamicGroup.fetch')}}",
                        method: "GET",
                        data: ({
                            select: select,
                            value: value,
                            dependent: dependent,
                        }),
                        success: function (groupResult) {
                            console.log("Result: "+groupResult);
                            $('#'+dependent).html(groupResult);
                        }
                    })
                }
            });

           //function to check the disable cell
           $('.btn-light').on('click', function(event){
                var thisCell = $(this);
                var selectedCell = thisCell;
                if(selectedCell.is('disable')){
                    console.log('button is diable');
                } else {
                    console.log('button is enable');
                    var room_time =  $(this).attr('data-time');
                    var room_seats = $(this).attr('data-room_seats');

                    event.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'GET',
                        url: "/buildings/room/schedule/"+room_time,
                        data: ({
                                room_time: room_time,
                                room_seats: room_seats,
                        }),
                        success: function(result){
                            // console.log(result);
                            $('#class_id').html(result);
                        }
                    });
                }
           })

           // if using ajax
           // function to disable the cell after assigned
           $('#save_assign').on('click', function(){
                selectedcell.css("background-color", "red");
                selectedcell.attr('disable', true);
           });

           // function to disable cell when render
           function diableCell(){
                selectedcell.css("background-color", "red");
                selectedcell.attr('disable', true);
           }



        });


        </script>

@endsection
