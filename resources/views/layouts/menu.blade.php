
<li class="nav-item {{ Request::is('buildings*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('home') !!}"><i  class="nav-icon icon-home"></i><span>Home</span></a>
</li>
@can('create', App\User::class)
<li class="nav-item {{ Request::is('buildings*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('buildings.index') !!}"><i class="nav-icon icon-organization"></i><span>Buildings</span></a>
</li>
<li class="nav-item {{ Request::is('classes*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('ShowAllCreateClass') !!}"><i class="nav-icon icon-clock"></i><span>Classes</span></a>
</li>
@endcan
@cannot('create', App\User::class)
<li class="nav-item {{ Request::is('classes*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('ProfessorClasses.index') !!}"><i class="nav-icon icon-clock"></i><span>Professor Classes</span></a>
</li>
@endcannot

@can('create', App\User::class)
<li class="nav-item {{ Request::is('usedRoom*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('ShowAllUsedRoom') !!}"><i class="nav-icon icon-cursor"></i><span>Used Rooms</span></a>
</li> 
@endcan

@can('create', App\User::class)
<li class="nav-item {{ Request::is('department*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('departments.index') !!}"><i class="nav-icon icon-graduation"></i><span>Departments</span></a>
</li>
@endcan

@can('create', App\User::class)
<li class="nav-item {{ Request::is('faculty*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('faculties.index') !!}"><i class="nav-icon icon-layers"></i><span>Faculties</span></a>
</li>
@endcan

@can('create', App\User::class)  
<li class="nav-item {{ Request::is('professor*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('professors.index') !!}"><i class="nav-icon icon-people"></i><span>Professors</span></a>
</li>
@endcan

@can('create', App\User::class)
<li class="nav-item {{ Request::is('subject*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('subjects.index') !!}"><i class="nav-icon icon-docs"></i><span>Subjects</span></a>
</li>
@endcan
{{--<li class="nav-item {{ Request::is('rooms*') ? 'active' : '' }}">--}}
{{--    <a class="nav-link" href="{!! route('rooms.index') !!}"><i class="nav-icon icon-cursor"></i><span>Rooms</span></a>--}}
{{--</li>--}}
{{--<li class="nav-item {{ Request::is('floors*') ? 'active' : '' }}">--}}
{{--    <a class="nav-link" href="{!! route('floors.index') !!}"><i class="nav-icon icon-cursor"></i><span>Floors</span></a>--}}
{{--</li>--}}

