@extends('layouts.app')

@section('content')

<style>
    .uper {
      margin-top: 30px;
    }
  </style>

<div class="uper">
  <div class="container-fluid">
      <div class="animated fadeIn">
          <div class="row">
              <div class="col-lg-12">
                  <div class="card">
                      <div class="card-header">
                          <h2>Lecturer Update Form:</h2>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive-sm">
                            <form method="post" action="{{ route('ProfessorClasses.update', ['ProfessorClass' => $classes->id]) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-group">
                                        @csrf
                                        <label for="formGroupExampleInput">Teacher Name:</label>
                                        <input class="form-control" name="teacher_name" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="{{$classes->user_dept->user->name}}"/>
                                </div>
                                <div class="form-group">
                                        @csrf
                                        <label for="formGroupExampleInput">Department:</label>
                                        <select class="form-control" name="prof_dept_id" >
                                        <option  value="{{$classes->user_dept_id}}" selected>{{$classes->user_dept->department->name}}</option>
                                            @foreach ($prof_depts as $prof_dept)
                                                @if ($classes->user_dept->department->name !== $prof_dept->department->name)
                                                    <option name="dep" value={{$prof_dept->id}}>{{$prof_dept->department->name}}</option>                                                    
                                                @endif
                                            @endforeach
                                        </select>
                                </div>
                                <div class="form-group">
                                        @csrf
                                        <label for="formGroupExampleInput">Subject:</label>
                                        <select class="form-control" name="subject_id" >
                                            <option value="{{$classes->subject->id}}" selected>{{$classes->subject->name}}</option>
                                            @foreach ($subjects as $subject)
                                                @if ($classes->subject->name !== $subject->name)
                                                    <option value={{$subject->id}}>{{$subject->name}}</option>                                                    
                                                @endif
                                            @endforeach
                                        </select>
                                </div>
                                <div class="form-group">
                                        @csrf
                                        <label for="formGroupExampleInput">Days:</label>
                                        <select class="form-control" name="day_id" >
                                            <option value="{{$classes->day}}" selected>{{$classes->day}}</option>
                                            @foreach ($days as $day)
                                                @if ($classes->day !== $day->name)
                                                    <option value={{$day->name}}>{{$day->name}}</option>                                                    
                                                @endif
                                            @endforeach
                                        </select>
                                </div>
                                <div class="form-group">
                                    @csrf
                                    <label for="formGroupExampleInput">Start Time:</label>
                                    <select class="form-control" name="start_time" >
                                        <option value="{{$classes->start_time}}" selected>{{$classes->start_time}}</option>
                                        @foreach ($times as $time)
                                            @if ($classes->start_time !== $time->time)
                                                <option value={{$time->time}}>{{$time->time}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    @csrf
                                    <label for="formGroupExampleInput">End Time:</label>
                                    <select class="form-control"name="end_time" >
                                        <option value="{{$classes->end_time}}" selected>{{$classes->end_time}}</option>
                                        @foreach ($times as $time)
                                            @if ($classes->end_time !== $time->time)
                                                <option value={{$time->time}}>{{$time->time}}</option>                                            
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                          </div>
    
                          <div class="pull-right mr-3">
    
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>

@can('create', User::class)
<div class="uper">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h2>Lecturer Update Form:</h2>
                            {{-- <a class="pull-right" href="{!! route('buildings.create') !!}">Add Building <i class="fa fa-plus-square fa-lg"></i></a> --}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive-sm">
                                <form method="post" action="{{ route('UpdateClassCreate',$classes->id) }}">
                                    <div class="form-group">
                                            @csrf
                                            <label for="formGroupExampleInput">Teacher Name:</label>
                                            <input class="form-control" name="teacher_name" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="{{$classes->user_dept->user->name}}"/>
                                    </div>
                                    <div class="form-group">
                                            @csrf
                                            <label for="formGroupExampleInput">Department:</label>
                                            <select class="form-control" name="prof_dept_id" >
                                            <option  value="{{$classes->user_dept_id}}" selected>{{$classes->user_dept->department->name}}</option>
                                                @foreach ($prof_depts as $prof_dept)
                                                    <option name="dep" value={{$prof_dept->id}}>{{$prof_dept->department->name}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div class="form-group">
                                            @csrf
                                            <label for="formGroupExampleInput">Subject:</label>
                                            <select class="form-control" name="subject_id" >
                                                <option value="{{$classes->subject->id}}" selected>{{$classes->subject->name}}</option>
                                                @foreach ($subjects as $subject)
                                                    <option value={{$subject->id}}>{{$subject->name}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div class="form-group">
                                            @csrf
                                            <label for="formGroupExampleInput">Days:</label>
                                            <select class="form-control" name="day_id" >
                                                <option value="{{$classes->day}}" selected>{{$classes->day}}</option>
                                                @foreach ($days as $day)
                                                    <option value={{$day->day}}>{{$day->day}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div class="form-group">
                                        @csrf
                                        <label for="formGroupExampleInput">Start Time:</label>
                                        <select class="form-control" name="start_time" >
                                            <option value="{{$classes->start_time}}" selected>{{$classes->start_time}}</option>
                                            @foreach ($times as $time)
                                                <option value={{$time->time}}>{{$time->time}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        @csrf
                                        <label for="formGroupExampleInput">End Time:</label>
                                        <select class="form-control"name="end_time" >
                                            <option value="{{$classes->end_time}}" selected>{{$classes->end_time}}</option>
                                            @foreach ($times as $time)
                                                <option value={{$time->time}}>{{$time->time}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
        
                            <div class="pull-right mr-3">
        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endcan

@endsection
