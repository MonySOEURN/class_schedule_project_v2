@extends('layouts.app')

@section('content')
<style>
    .uper {
      margin-top: 30px;
    }
  </style>

@cannot('create', App\User::class)
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif
  <div class="container-fluid">
      <div class="animated fadeIn">
          <div class="row">
              <div class="col-lg-12">
                  <div class="card">
                      <div class="card-header">
                          <i class="fa fa-align-justify"></i>
                          Lecturer Available times:
                          <a href="{{ route('ProfessorClasses.create')}} " class="btn btn-primary" style="float: right;" ><i class="fa fa-plus-square fa-lg"></i> Add available time</a>
                          {{-- <a class="pull-right" href="{!! route('buildings.create') !!}">Add Building <i class="fa fa-plus-square fa-lg"></i></a> --}}
                      </div>
                      <div class="card-body">
                          <div class="table-responsive-sm">
                            <table class="table table-striped">
                              <thead>
                                  <tr style="border-top: none;">
                                      <td>No</td>
                                      <td>Subject</td>
                                      <td>Teacher's name</td>
                                      <td>Date</td>
                                      <td>Start Time</td>
                                      <td colspan="2" style="text-align: center;">Actions</td>
                                    </div>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach ($user_depts as $user_dept)
                                  @foreach($classes as $class)
                                    @if ($class->user_dept_id === $user_dept->id)
                                      <tr>
                                          <td>{{$loop->iteration}}</td>
                                          <td>{{$class->subject->name}}</td>
                                          <td>{{$class->user_dept->user->name}}</td>
                                          <td>{{$class->day}}</td>
                                            <td>{{$class->start_time}}</td>
                                          <td  style="text-align: right;"><a href="{{ route('ProfessorClasses.edit',$class->id)}}" class="btn btn-primary">Edit</a></td>
                                          <td>
                                              <form action="{{ route('ProfessorClasses.destroy', $class->id)}}" method="post">
                                                  {{ csrf_field() }}
                                                  {{ method_field('DELETE') }}
                                            <button class="btn btn-danger" type="submit" onclick="return confirm('Do you really want to delete Subject {{$class->subject->name}} ?')">Delete</button>
                                              </form>
                                          </td>
                                      </tr>
                                    @endif
                                  @endforeach
                                @endforeach
                              </tbody>
                            </table>
                          </div>
    
                          <div class="pull-right mr-3">
    
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>
@endcannot
@can('create', App\User::class)
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif
  <div class="container-fluid">
      <div class="animated fadeIn">
          <div class="row">
              <div class="col-lg-12">
                  <div class="card">
                      <div class="card-header">
                          <i class="fa fa-align-justify"></i>
                          Lecturer Available times:
                          <a href="{{ route('ClassesCreate')}} " class="btn btn-primary" style="float: right;" ><i class="fa fa-plus-square fa-lg"></i> Add available time</a>
                          {{-- <a class="pull-right" href="{!! route('buildings.create') !!}">Add Building <i class="fa fa-plus-square fa-lg"></i></a> --}}
                      </div>
                      <div class="card-body">
                          <div class="table-responsive-sm">
                                    <table class="table table-striped">
                                      <thead>
                                          <tr style="border-top: none;">
                                              <td>No</td>
                                              <td>Subject</td>
                                              <td>Teacher's name</td>
                                              <td>Total Student</td>
                                              <td>Date</td>
                                              <td>Start Time</td>
                                              <td colspan="2" style="text-align: center;">Actions</td>
                                            </div>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          @foreach($classes as $class)
                                          <tr>
                                              <td>{{$loop->iteration}}</td>
                                              <td>{{$class->subject->name}}</td>
                                              <td>{{$class->prof_name}}</td>
                                              <td>{{$class->total_students}}</td>
                                              <td>{{$class->day}}</td>
                                                <td>{{$class->start_time}}</td>
                                              <td  style="text-align: right;"><a href="{{ route('EditClassesCreate',$class->id)}}" class="btn btn-primary">Edit</a></td>
                                              <td>
                                                  <form action="{{ route('DeleteClassesCreate', $class->id)}}" method="post">
                                                    @csrf
                                                <button class="btn btn-danger" type="submit" onclick="return confirm('Do you really want to delete Subject {{$class->subject->name}} ?')">Delete</button>
                                                  </form>
                                              </td>
                                          </tr>
                                          @endforeach
                                      </tbody>
                                    </table>
                          </div>
    
                          <div class="pull-right mr-3">
    
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>
@endcan




@endsection
