@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Department
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('departments.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Department Name:</label>
              <input type="text" class="form-control" name="name"/>

              <label for="name">Faculty Name:</label>
              <select name="faculty_id" class="form-control">
                <option value="">Select Faculties</option>    
                @foreach ($faculties as $faculty)
                  <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                @endforeach
              </select>
          </div>
          
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@endsection
