<html lang="en">
<head>

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="utf-8"/>

    <style>
        .table td{
            border-top: none;
        }
        label {
            margin-bottom: none;
        }
        .countryFlag{
            text-align: right;
            padding-right: 80px; 
            padding-top: 20px; 
            font-size: 24px; 
            font-weight: bold;
        }
        body {
            font-family: 'khmerosbokor', sans-serif;
        }
    </style>
</head>

<body>

    <div style="margin: 10px 10px 20px 5px;">
        <button type="submit" class="btn btn-primary" style="display: inline;"><a href="{{ URL('/'.$semester.'/'.$department->id.'/'.$group->name.'/download/schedules' )}}" style="color:white;" >Download Schedule</a></button>
        <button type="submit" class="btn btn-primary" style="display: inline; float: right; "><a href="{{ URL('buildings/'.$building_id.'/schedule' )}}" style="color:white;">Back</a></button>
    </div>
    
    
    <div size="A4">
        <div class="sub-container" style="margin: 30px;">
            <div class="shadow-lg p-3 bg-white rounded">
            <div>
                <table style="width:100%">
                    <tr>
                        <th></th>
                        <th class="countryFlag">ព្រះរាជាណាចក្រកម្ពុជា</th> 
                    </tr>
                    <tr>
                        <th style="font-size: 24px;">ក្រសួងអប់រំ យុវជន និងកីឡា</th>
                        <th class="countryWord" style="text-align: right;padding-right: 50px; font-size: 24px;">ជាតិ សាសនា ព្រះមហាក្សត្រ</th>
                    </tr>
                    <tr>
                        <th style="font-size: 24px;">សាកលវិទ្យាល័យភូមិន្ទភ្នំពេញ</th>
                    </tr>
                    <tr>
                        <th style="font-size: 24px;">មហាវិទ្យាល័​{{$faculty->name}}</th>
                    </tr>
                </table>
            </div>
            <div style="text-align:center; ">
                <h5 style="font-weight: bold;">កាលវិភាគឆមាសទី {{$semester}}</h5>
                <h5 style=" font-weight: bold;">ឆ្នាំទី១​ ដេប៉ាតឺម៉ង់ {{$department->name}} + ក្រុម {{$group->name}}</h5>
                <p>ចាប់ពីថ្ងៃ.......... ...រោច ខែ.......... ដល់ថ្ងៃ........... ...រោច ខែ.......... ឆ្នាំ........................ព.ស ........ </p>
                <p>ចាប់ពីថ្ងៃទី..... ខែ.......... ឆ្នាំ............ ដល់ថ្ងៃទី...... ខែ.......... ឆ្នាំ............ </p>
            </div>
            <div style="text-align:left; ">
                <p style=" font-weight: bold;">ចំនួននិស្សិត.......  បន្ទប់រៀន {{$room->name}}</p>
            </div>
            <div>
                <table id="customers" class="table table-bordered" style="text-align: center;">
                    <thead>
                        <tr>
                            <th style="width: 120px;">Time & Day</th>
                            @foreach($days as $day)
                                <th>{{$day->name}}</th>
                            @endforeach
                        </tr>
                    </thead>
         
                    <tbody>
                        @foreach($times as $time)
                            <tr>
                                <th>{{$time->time}}</th>
                                @foreach($days as $day)
                                    <td style="padding: 0px;height: 51px; width: 221px;">
        
                                        <?php $assigned = false; ?>
                                        @foreach ($assigned_rooms as $assigned_room)
                                            @if ($assigned_room->start_time === $time->time && $assigned_room->day->name === $day->name && $assigned_room->room->name === $room_name)
                                                <button type="button" data-time="{{$time->time}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->room_name}}"  class="btn btn-light  btn-xs col-lg-12" style="height: 51px; width: 221px;white-space: normal;font-size: 12px;border-radius:0px;background-color:#dc3545; border-color:#dc3545; color:white;" data-toggle="modal" data-target="#myModal" id="myButton" disabled="true" >
                                                    {{$assigned_room->class->subject->name}}
                                                    <?php $assigned = true; ?>
                                                </button>
                                            @endif
                                        @endforeach
                                        @if ($assigned === false)
                                            <button type="button" data-room_seats={{$room->total_students}} data-time="{{$time->time}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->room_name}}"  class="btn btn-light" style="height: 51px; width: 221px;white-space: normal; font-size: 12px;border-radius:0px;" data-toggle="modal" data-target="#myModal" id="myButton">
                                            </button>
                                        @endif
                                    </td>
                                @endforeach
        
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="table-responsive-sm">
                <table class="table">
                    <tr style="font-size:15px;">
                        <td class="th-lg" style="width: 20%;border-top: none; padding: none;">
                            <table style="font-size: 13px;border-top: none;">
                                <tr style="padding: 0px;">
                                    <td style="width: 5%;padding: 0px;">1</td>
                                    <td style="text-align: left; width: 65%;padding: 0px;">Game Developement manager</td>
                                    <td style="text-align: right;width: 20%;padding: 0px;">3 (3 - 0)</td>
                                </tr>
                                <tr style="padding: 0px;">
                                    <td style="width: 5%;padding: 0px;">1</td>
                                    <td style="text-align: left; width: 65%;padding: 0px;">Game Developement manager</td>
                                    <td style="text-align: right;width: 20%;padding: 0px;">3 (3 - 0)</td>
                                </tr>
                                <tr style="padding: 0px;">
                                    <td style="width: 5%;padding: 0px;">1</td>
                                    <td style="text-align: left; width: 65%;padding: 0px;">Game Developement manager</td>
                                    <td style="text-align: right;width: 20%;padding: 0px;">3 (3 - 0)</td>
                                </tr>
                                <tr style="padding: 0px;">
                                    <td style="width: 5%;padding: 0px;">1</td>
                                    <td style="text-align: left; width: 65%;padding: 0px;">Game Developement manager</td>
                                    <td style="text-align: right;width: 20%;padding: 0px;">3 (3 - 0)</td>
                                </tr>
                                <tr style="padding: 0px;">
                                    <td style="width: 5%;padding: 0px;">1</td>
                                    <td style="text-align: left; width: 65%;padding: 0px;">Game Developement manager</td>
                                    <td style="text-align: right;width: 20%;padding: 0px;">3 (3 - 0)</td>
                                </tr>
                                <tr style="padding: 0px;">
                                    <td style="width: 5%;padding: 0px;">1</td>
                                    <td style="text-align: left; width: 65%;padding: 0px;">Game Developement manager</td>
                                    <td style="text-align: right;width: 20%;padding: 0px;">3 (3 - 0)</td>
                                </tr>
                                <tr style="padding: 0px;">
                                    <td style="width: 5%;padding: 0px;">1</td>
                                    <td style="text-align: left; width: 65%;padding: 0px;">Game Developement manager</td>
                                    <td style="text-align: right;width: 20%;padding: 0px;">3 (3 - 0)</td>
                                </tr>
                                <tr style="padding: 0px;">
                                    <td style="width: 5%;padding: 0px;">1</td>
                                    <td style="text-align: left; width: 65%;padding: 0px;">Game Developement manager</td>
                                    <td style="text-align: right;width: 20%;padding: 0px;">3 (3 - 0)</td>
                                </tr>
                            </table>
                        </td>
                        <td class="th-lg" style="border-top: none;text-align: center;">
                            <label>បានឃើញ និងយល់ព្រម</label><br>
                            <label>ជ.សាកលវិទ្យាធិការ</label><br>
                            <label>សាកលវិទ្យាធិការរង</label>
                        </td>
                        <td class="th-lg" style="border-top: none;text-align: center;">
                            <label>បានឃើញ​ និងពិនិត្យត្រឹមត្រូវ</label><br>
                            <label>ព្រឹទ្ធបុរសមហាវិទ្យាល័យ. {{$department->name}}</label>
                        </td>
                        <td class="th-lg" style="border-top: none;text-align: center;">
                            <label>បានឃើញ​ និងពិនិត្យត្រឹមត្រូវ</label><br>
                            <label>ប្រធានការិយាល័យសិក្សា</label>
                        </td>
                        <td class="th-lg" style="border-top: none; text-align: center;">
                            <label>ថ្ងៃព្រហស្បត្តិ៍​ ៩រោច​ ខែអាសាឍ ឆ្នាំកុរ ឯកស័ក ព.ស ២៥៦៣</label><br>
                            <label>រាជធានីភ្នំពេញ ថ្ងៃទី២៥ ខែកក្កដា ឆ្នាំ២០១៩</label><br>
                            <label>ប្រធានដេប៉ា. {{$department->name}}</label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="text-align:right; height: 120px;">
            </div>
        </div>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>

</html>
