@extends('layouts.app')

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{!! route('buildings.index') !!}">All Building</a>
        </li>
        <li class="breadcrumb-item active">{{ $building->name }}</li>

    </ol>

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="pull-right" href="{{ route('floors.create', ['building_id' => $building->id]) }}">Add Floor <i class="fa fa-plus-square fa-lg"></i></a>
                            {{-- <strong>{{ $rooms->name }}</strong> --}}
                            <a href="{{ route('building.schedule', ['id' => $building->id ])}}" class="btn btn-primary" style="margin-left: 450px;">View Schedule of Building</a>
                            <a href="{!! route('buildings.index') !!}" class="btn btn-ghost-light">Back</a>
                        </div>
                        <div class="card-body">
                            @foreach($floors as $floor)
                                <a class="pull-right m-2" href="{!! route('rooms.create', $floor->id) !!}">Add Room <i class="fa fa-plus-square fa-lg"></i></a>
                                <div class="card-header">
                                    {{ $floor->name }}
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive-sm">
                                        <div class="box">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    @foreach ($floor->rooms as $room)
                                                        @if ($room->building_id === $building->id)
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

                                                                <div class="card" style="max-width: 20rem;">
                                                                    <h3 class="card-header">
                                                                        {{ $room->name }}
                                                                    </h3>
                                                                    <div class="card-body">
                                                                        {{--                                <p class="card-text">Total room:{!! $building->total_rooms !!}</p>--}}
                                                                        <p class="card-text">Total Students  :{!! $room->total_students !!}</p>
                                                                        {{--                                <p class="card-text">Total room per floor:{!! $building->total_rooms_per_floor !!}</p>--}}
                                                                    </div>
                                                                    <div class="card-footer">
                                                                        <div class='btn-group'>
                                                                            {!! Form::open(['route' => ['ShowRoomSchedule', $building->id, $room->id], 'method' => 'get']) !!}
                                                                                {!! Form::button('<i class="fa fa-eye"  title="Edit Room"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-success', 'style' => 'height: 40px;' ]) !!}
                                                                            {!! Form::close() !!}
                                                                            @if ($room->used_room === 0)
                                                                                {!! Form::open(['route' => ['AddUseRoom', $building->id, $room->id], 'method' => 'put']) !!}
                                                                                    {!! Form::button('<i class="fa fa-plus-square"  title="Use Room"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-info', 'style' => 'color:green; height: 40px;']) !!}
                                                                                {!! Form::close() !!}
                                                                            @endif
                                                                            {!! Form::open(['route' => ['rooms.edit', $room->id], 'method' => 'edit']) !!}
                                                                                {!! Form::button('<i class="fa fa-edit"  title="Edit Room"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-info', 'style' => 'height: 40px;' ]) !!}
                                                                            {!! Form::close() !!}
                                                                            {!! Form::open(['route' => ['rooms.destroy', $room->id], 'method' => 'delete']) !!}
                                                                                {!! Form::button('<i class="fa fa-trash"  title="Delete Room"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'style' => 'height: 40px;', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                                            {!! Form::close() !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    $( window ).on( "load", readyFn ){
        console.log('loaded');
    };
</script>
