{{-- {{-- @extends('layout') --}}

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta name="csrf-token" content="{{ csrf_token() }}" />
{{-- ajax --}}
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

{{-- @section('content') --}}

@if ($errors->any())
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ implode('', $errors->all(':message ')) }}</strong>
    </div>
@endif 
@if(session()->has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ session()->get('message') }}
    </div>
@endif
<div style="margin: 10px 10px 20px 5px;">
    <h3 style="display: inline;">Schedule : {{$building->name}}</h3>
    <!-- href="{{ URL('/'.$building->id.'/download/schedules' )}}" -->
    {{--  <button type="submit" class="btn btn-primary" style="display: inline;"><a href="{{ URL('/print/schedules' )}}" style="color:white;" >Download Schedule for view</a></button>  --}}
    <button type="submit" class="btn btn-primary" style="display: inline;"><a  style="color:white;" data-toggle="modal" data-target="#pdfModal" data-building_id={{$building->id}}>Print Schedule</a></button>
    <button type="submit" class="btn btn-primary" style="display: inline; float: right; "><a href="{{ URL('buildings/'.$building->id )}}" style="color:white;">Back</a></button>
    <div style="display: flex;align-items: center;justify-content: center">
            <label><u>S</u>elect Schedules: </label>
            <select name="select_show_room" style="margin-left: 10px;" onchange="top.location.href = this.options[this.selectedIndex].value">
                    <option value="">Schedule options</option>
                    <option value="{{ route("building.schedule", ["id" => $building->id]) }}">Building Schedule</option>
                @foreach ($rooms as $room)
                    <option value="{{ route("ShowRoomSchedule", ["building_id" => $building->id, "room_id" => $room]) }}">{{$room->name}}</option>
                @endforeach
            </select>
    </div>
</div>

<div>
    <table class="table table-bordered" style="font-size:10px;text-align:center;vertical-align: middle;">
        <thead>
            <tr>
            {{-- span of room number column --}}
                <th rowspan="2" colspan="2">Room No:</th>
                {{-- show days --}}
                @foreach ($days as $day)
                    <th class="mainDay  _{{$day->name}}" colspan="3">{{$day->name}}</th>
                @endforeach
            </tr>
            <tr> 
            {{-- show day by part such as Morning... --}}
                @foreach ($days as $day)
                    @foreach ($day_parts as $day_part)
                    <td class="shiftDay_{{$day->day}}_{{$day_part->name}}">
                        {{$day_part->name}}
                    </td>
                    @endforeach
                @endforeach
            </tr>
        </thead>
        <tbody>

            @foreach ($rooms as $room)
                <tr>
                    <th class="room_{{$room->name}}" rowspan="10" style="vertical-align: middle;">
                        {{$room->name}}
                    </th>
                    @php
                        $row_val = 0;
                        $col_val = 0;
                        $i = 0; 
                        $row_span_num = null;
                        $array_spans[10][6]= -1 ;
                        $M_array_compare[10][6] = -1;
                        $N_array_compare[10][6] = -1;
                        $E_array_compare[10][6] = -1;
                        for ($row = 0; $row < 10; $row++)
                        {
                            for ($col = 0; $col < 6; $col++)
                            {
                                $array_spans[$row][$col] = -1;
                                $M_array_compare[$row][$col] = -1;
                                $N_array_compare[$row][$col] = -1;
                                $E_array_compare[$row][$col] = -1;
                            }
                        }
                    @endphp
                    @foreach ($display_times as $display_time)
                        @if ($i%2 === 0)
                            <th rowspan="2">{{$display_time->morning}}</th>
                        @endif
                        @php
                             $col_val = 0;
                        @endphp
                        @foreach ($days as $day)
                            @php
                                $assigned_morning = false; 
                                $assigned_noon = false; 
                                $assigned_evening = false;
                                $assigned = false;
                            @endphp
                            @foreach ($assigned_rooms as $assigned_room)
                                @if ($assigned_room->start_time === $display_time->morning && $assigned_room->day->name === $day->name && $assigned_room->room->name === $room->name)
                                    <td rowspan="{{$assigned_room->row_span_num}}"  style="background-color:#dc3545;font-size: 8px;padding:0; height: 29px; width: 85px;white-space: normal;word-wrap: break-word;">
                                        <button type="button" data-time="{{$display_time->morning}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->name}}"  class="btn btn-light  btn-xs col-lg-12" style="padding: 0px;font-size: 8px;border-radius:0px; width:86px; height:29px; background-color:#dc3545; border-color:#dc3545; color:white;" data-toggle="modal" data-target="#myModal" id="myButton" disabled="true" >
                                                {{$assigned_room->class->subject->name}}
                                            @php 
                                                $assigned_morning = true; 
                                            @endphp
                                            @for($j = 0; $j< $assigned_room->row_span_num; $j++)
                                                @php
                                                    $tmp = $row_val;
                                                    $M_array_compare[$tmp+$j][$col_val] = $assigned_room->row_span_num-$j;
                                                @endphp
                                            @endfor
                                        </button>
                                    </td>
                                @endif
                            @endforeach

                            @if ($row_val < 10 )
                                @if ($assigned_morning === false && $M_array_compare[$row_val][$col_val] === -1)                                    
                                    <td  style="font-size: 8px;padding:0; white-space: normal; height: 29px; width: 85px; font-size:5px;">
                                        <button type="button" data-room_seats={{$room->total_students}} data-time="{{$display_time->morning}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->name}}"  class="btn btn-light" style="font-size: 8px;border-radius:0px;width:86px; height:29px;" data-toggle="modal" data-target="#myModal" id="myButton">
                                        </button>
                                    </td>
                                @endif
                            @endif

                        <!-- afternoon -->
                        @foreach ($assigned_rooms as $assigned_room)
                            @if ($assigned_room->start_time === $display_time->afternoon && $assigned_room->day->name == $day->name && $assigned_room->room->name == $room->name)
                                
                                <td rowspan="{{$assigned_room->row_span_num}}"  style="background-color:#dc3545;font-size: 8px;padding:0; height: 29px; width: 85px;white-space: normal;word-wrap: break-word;">
                                    <button type="button" data-time="{{$display_time->morning}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->name}}"  class="btn btn-light  btn-xs col-lg-12" style="padding: 0px;font-size: 8px;border-radius:0px; width:86px; height:29px; background-color:#dc3545; border-color:#dc3545; color:white;" data-toggle="modal" data-target="#myModal" id="myButton" disabled="true" >
                                        {{$assigned_room->class->subject->name}}
                                        @php 
                                            $assigned_noon = true; 
                                        @endphp
                                        @for($j = 0; $j< $assigned_room->row_span_num; $j++)
                                            @php
                                                $tmp = $row_val;
                                                $N_array_compare[$tmp+$j][$col_val] = $assigned_room->row_span_num-$j;
                                            @endphp
                                        @endfor
                                    </button>
                                </td>
                            @endif
                        @endforeach

                        @if ($row_val < 10 )
                            @if ($assigned_noon === false && $N_array_compare[$row_val][$col_val] === -1)                                    
                                <td  style="font-size: 8px;padding:0; white-space: normal; height: 29px; width: 85px; font-size:5px;">
                                    <button type="button" data-room_seats={{$room->total_students}} data-time="{{$display_time->morning}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->name}}"  class="btn btn-light" style="font-size: 8px;border-radius:0px;width:86px; height:29px;" data-toggle="modal" data-target="#myModal" id="myButton">
                                    </button>
                                </td>
                            @endif
                        @endif

                        <!-- evening -->
                        @foreach ($assigned_rooms as $assigned_room)
                            @if ($assigned_room->start_time === $display_time->evening && $assigned_room->day->name == $day->name && $assigned_room->room->name == $room->name)
                            <td rowspan="{{$assigned_room->row_span_num}}"  style="background-color:#dc3545;font-size: 8px;padding:0; height: 29px; width: 85px;white-space: normal;word-wrap: break-word;">
                                <?php $assigned = false; ?>
                                <button type="button" data-time="{{$display_time->morning}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->name}}"  class="btn btn-light  btn-xs col-lg-12" style="padding: 0px;font-size: 8px;border-radius:0px;width:86px; height:28px; background-color:#dc3545; border-color:#dc3545; color:white;" data-toggle="modal" data-target="#myModal" id="myButton" disabled="true" >
                                    {{$assigned_room->class->subject->name}}
                                    @php 
                                        $assigned_evening = true; 
                                    @endphp
                                    @for($j = 0; $j< $assigned_room->row_span_num; $j++)
                                        @php
                                            $tmp = $row_val;
                                            $E_array_compare[$tmp+$j][$col_val] = $assigned_room->row_span_num-$j;
                                        @endphp
                                    @endfor
                                    </button>
                                </td>
                            @endif
                        @endforeach

                        @if ($row_val < 10 )
                            @if ($assigned_evening === false && $E_array_compare[$row_val][$col_val] === -1)                                    
                                <td  style="font-size: 8px;padding:0; white-space: normal; height: 29px; width: 85px; font-size:5px;">
                                    <button type="button" data-room_seats={{$room->total_students}} data-time="{{$display_time->morning}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->name}}"  class="btn btn-light" style="font-size: 8px;border-radius:0px;width:86px; height:29px;" data-toggle="modal" data-target="#myModal" id="myButton">
                                    </button>
                                </td>
                            @endif
                        @endif

                        @php
                            $col_val++;
                        @endphp
                        @endforeach
                        @php
                            $i++;
                            $row_val++;
                        @endphp
                </tr>
                    @endforeach
            @endforeach
            

        </tbody>
    </table>
</div>


<div id="myModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="text-align:center;">
            <div class="modal-header" style="background-color:#3700B3; color:white;">
                <h5 class="modal-title" id="exampleModalLabel" style="margin: 0 auto;">ASSIGN CLASSROOM DIALOG</h5>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white;">
                    <span aria-hidden="true">&times;</span>
                </button> --}}
            </div>
            <div class="modal-body">
                <form action="{{route('BuildingStoreAssignedClass',['id'=> $building->id])}}" method="POST">
                    {{csrf_field()}}

                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Department </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control dynamic" id="department_name" name="department_id" data-department="group">
                                <option value="">Select Department</option>
                                <!-- to display data after group by method -->
                                @foreach ($departments as $department )
                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Groups Type </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control dynamic" id="group" name="group_id" data-department="group_name">
                                <option value="">Select Groups Type</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Class </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control" id="class_id" name="class_id" placeholder="Class Name">
                                <option id="class_init_opt" value="">Select Subject</option>
                                @foreach ($classes as $class)
                                    {{-- @if ($class->time == time) --}}
                                        <option value="{{$class->id}}">{{$class->subject->name}}</option>
                                    {{-- @endif --}}
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Day </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control" id="day_id" name="day_id" placeholder="Class Name">
                                <option id="day_init_opt" value="">Teaching Day</option>
                                <!-- @foreach ($days as $day)
                                    <option value="{{$day->id}}">{{$day->name}}</option>
                                @endforeach -->
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Start </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control" id="start_time" name="start_time" placeholder="Class Name">
                                    <option id="start_init_opt" value="">Start Time</option>
                                @foreach ($times as $time)
                                    <option value="{{$time->time}}">{{$time->time}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">End </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control" id="end_time" name="end_time" placeholder="End Time">
                                <option id="end_init_opt" value="">End Time</option>
                                @foreach ($times as $time)
                                    <option value="{{$time->time}}">{{$time->time}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- hidden input --}}
                    <input type="hidden" class="room_id" name="room_id" id="room_id" value="null">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    <button type="submit" id="save_assign" class="btn btn-primary" style="margin-left: 50px;background-color: #7db300;border-color:#7db300;">Save Assign</button>

                </form>
            </div>
        </div>
    </div>
</div>

<div id="pdfModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="text-align:center;">
            <div class="modal-header" style="background-color:#3700B3; color:white;">
                <h5 class="modal-title" id="exampleModalLabel" style="margin: 0 auto;">ASSIGN CLASSROOM DIALOG</h5>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white;">
                    <span aria-hidden="true">&times;</span>
                </button> --}}
            </div>
            <div class="modal-body">
                <form action="{{route('print.schedule',['building_id'=> $building->id])}}" method="GET">


                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Semester </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control" id="semester" name="semester">
                                    <option id="" value="">Select Semester</option>
                                    <option value="1">1</option>
                                    <option value="1">2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                            <label class="col-sm-2 col-form-label">Study Shift </label>
                            <div class="col-sm-10">
                                <select type="text" class="form-control" id="time_shift" name="time_shift">
                                        <option id="" value="">Select Time shift</option>
                                        <option value="1">Morning</option>
                                        <option value="2">Afternoon </option>
                                        <option value="3">Evening</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Room </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control" id="room_id" name="room_id">
                                    <option id="start_init_opt" value="">Select Room</option>
                                    @foreach ($rooms as $room)
                                        <option value="{{$room->id}}">{{$room->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Faculty </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control dynamic" id="faculty_name2" name="faculty_id" data-department="department_name2">
                                {{-- <select type="text" class="form-control" id="room_id" name="room_id"> --}}
                                    <option id="start_init_opt" value="">Select Faculty</option>
                                    @foreach ($faculties as $faculty)
                                        <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Department </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control dynamic" id="department_name2" name="department_id" data-department="group2">
                                <option value="">Select Department</option>
                                <!-- to display data after group by method -->
                                @foreach ($departments as $department )
                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="margin: 40 0 40 50px; width: 600px; height: 50px;margin-bottom: 40px; font-weight:bold;">
                        <label class="col-sm-2 col-form-label">Groups Type </label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control dynamic" id="group2" name="group_id" data-department="group_name2">
                                <option value="">Select Groups Type</option>
                            </select>
                        </div>
                    </div>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    <button type="submit" id="save_assign" class="btn btn-primary" style="margin-left: 50px;background-color: #7db300;border-color:#7db300;">View Print</button>

                </form>
            </div>
        </div>
    </div>
</div>

{{-- bootstrap --}}
{{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script>
$(document).ready(function(){

    var getSelectedTime;
    // function to get the event of click and send to modal for assign room
    $('#myModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget)
        var day = button.data('day');
        var room_id = button.data('room_id');
        var day_id = button.data('day_id');
        var time = button.data('time');

        var modal =$(this); // get the this modal DOM

        modal.find('form #day_init_opt').val(day_id)
        modal.find('form #day_init_opt').text(day)
        modal.find('form #room_id').val(room_id)
        modal.find('form if').val(time)


        // dependent selection from from department to group
        $('#department_name').change(function () {
            if($(this).val() != ''){
                var select = $(this).attr( 'id');
                var value = $(this).val();
                var dependent = $(this).data('department');
                console.log(select);
                console.log(value);
                console.log(dependent);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('dynamicGroup.fetch')}}",
                    method: "GET",
                    data: ({
                        select: select,
                        value: value,
                        dependent: dependent,
                    }),
                    success: function (groupResult) {
                        console.log("Result: "+groupResult);
                        $('#'+dependent).html(groupResult);
                    }
                })
            }
        });
   })

   $('#pdfModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget)
        var day = button.data('day');
        var room_id = button.data('room_id');
        var day_id = button.data('day_id');
        var time = button.data('time');

        var modal =$(this); // get the this modal DOM

        modal.find('form #day_init_opt').val(day_id)
        modal.find('form #day_init_opt').text(day)
        modal.find('form #room_id').val(room_id)
        modal.find('form if').val(time)


        // dependent selection from from department to group
        $('#faculty_name2').change(function () {
            if($(this).val() != ''){
                var select = $(this).attr( 'id');
                var value = $(this).val();
                var dependent = $(this).data('department');
                console.log(select);
                console.log(value);
                console.log(dependent);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('dynamicDepartment.fetch')}}",
                    method: "GET",
                    data: ({
                        select: select,
                        value: value,
                        dependent: dependent,
                    }),
                    success: function (groupResult) {
                        console.log("Result: "+groupResult);
                        $('#'+dependent).html(groupResult);
                    }
                })
            }
        });

        $('#department_name2').change(function () {
            if($(this).val() != ''){
                var select = $(this).attr( 'id');
                var value = $(this).val();
                var dependent = $(this).data('department');
                console.log(select);
                console.log(value);
                console.log(dependent);
                // var _token = $('input[name = "_token"]').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('dynamicGroup.fetch')}}",
                    method: "GET",
                    data: ({
                        select: select,
                        value: value,
                        dependent: dependent,
                    }),
                    success: function (groupResult) {
                        console.log("Result: "+groupResult);
                        $('#'+dependent).html(groupResult);
                    }
                })
            }
        });

    })


   //function to check the disable cell
   $('.btn-light').on('click', function(event){
        var thisCell = $(this);
        var selectedCell = thisCell;
        if(selectedCell.is('disable')){
            console.log('button is diable');
        } else {
            console.log('button is enable');
            var room_time =  $(this).attr('data-time');
            var room_seats = $(this).attr('data-room_seats');
            var day = $(this).attr('data-day');

            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: "/buildings/schedule/"+room_time,
                data: ({
                        room_time: room_time,
                        room_seats: room_seats,
                }),
                success: function(result){
                    $('#class_id').html(result);
                }
            });

        }
    })

   // if using ajax
   // function to disable the cell after assigned
    $('#save_assign').on('click', function(){
        selectedcell.css("background-color", "red");
        selectedcell.attr('disable', true);
    });

   // function to disable cell when render
    function diableCell(){
        selectedcell.css("background-color", "red");
        selectedcell.attr('disable', true);
    }

});


</script>
