<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="utf-8"/>
    <style>
        #customers, .table,  td{
            border: 1px solid black;
            border-collapse: collapse;
        }
        body ,header{
            font-family: 'khmerosmuollight' , sans-serif;
        }
        footer{
            font-size: 72px;
        }
        #new-customer{}
    </style>
</head>
<body>
    
        <div>
            <div class="sub-container" style="margin: 30px;">
                <div class="shadow-lg p-3 bg-white rounded">

                    {{--  header part  --}}
                    <header>
                        <div>
                            <table style="width:100%; border:none; ">
                                <tr>
                                    <td style="border:none; "></td>
                                    <td style="text-align: right;padding-right: 75px; font-size: 24px;border:none; ">ព្រះរាជាណាចក្រកម្ពុជា</td> 
                                </tr>
                                <tr>
                                    <td style="font-size: 24px;text-align: left;border:none; ">ក្រសួងអប់រំ យុវជន និងកីឡា</td>
                                    <td style="text-align: right;padding-right: 50px; font-size: 24px;border:none; ">ជាតិ សាសនា ព្រះមហាក្សត្រ</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 24px;text-align: left;border:none; ">សាកលវិទ្យាល័យភូមិន្ទភ្នំពេញ</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 24px;text-align: left;border:none; ">មហាវិទ្យាល័​យ{{$faculty->name}}</td>
                                </tr>
                            </table>
                        </div>
                    </header>

                    {{--  middle part  --}}
                    <div>
                        <div style="text-align:center; ">
                                <label style="font-weight: bold;">កាលវិភាគឆមាសទី {{$semester}}</label><br>
                                <label style="font-weight: bold;">ឆ្នាំទី១​ ដេប៉ាតឺម៉ង់ {{$department->name}} + ក្រុម {{$group->name}}</label><br>
                                <label>ចាប់ពីថ្ងៃ.......... ...រោច ខែ.......... ដល់ថ្ងៃ........... ...រោច ខែ.......... ឆ្នាំ........................ព.ស ........ </label><br>
                                <label>ចាប់ពីថ្ងៃទី..... ខែ.......... ឆ្នាំ............ ដល់ថ្ងៃទី...... ខែ.......... ឆ្នាំ............ </label>
                            </div>
                            <div style="text-align:left; ">
                                <p style=" font-weight: bold;">ចំនួននិស្សិត.......  បន្ទប់រៀន {{$room->name}}</p>
                            </div>
                            <div>
                                <table id="customers" class="table table-bordered" style="text-align: center; width: 100% ">
                                    <thead>
                                        <tr>
                                            <th style="width: 120px; border: 1px solid black;border-collapse: collapse; ">Time & Day</th>
                                            @foreach($days as $day)
                                                <th style="border: 1px solid black;border-collapse: collapse; ">{{$day->name}}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $is =0;
                                            $i = 0;
                                            $row_val = 0;
                                            $col_val = 0;
                                            $array_spans[32][6]= -1 ;
                                            $array_compare[32][7] = -1;
                                            for ($row = 0; $row < 32; $row++)
                                            {
                                                for ($col = 0; $col < 6; $col++)
                                                {
                                                    $array_spans[$row][$col] = -1;
                                                }
                                            }
                                            for ($row = 0; $row < 32; $row++)
                                            {
                                                for ($col = 0; $col < 7; $col++)
                                                {
                                                    $array_compare[$row][$col] = -1;
                                                }
                                            }
                                        @endphp
                        
                                        @foreach($times as $time)
                                        <?php 
                                            $assigned_end = false; 
                                            $assigned_day = false;
                                            $col_val = 0;
                                        ?>
                                            <tr>
                                                @if ($i%2 ===0)
                                                    <th style="border: 1px solid black;border-collapse: collapse; " rowspan="2">{{$time->time}}</th>
                                                @endif
                                                @foreach($days as $day)
                                                    @php
                                                        $assigned_flag = false;
                                                    @endphp
                                                    @foreach ($assigned_rooms as $assigned_room)
                                                        @if ($assigned_room->start_time === $time->time && $assigned_room->day->name === $day->name && $assigned_room->room->name === $room->name)
                                                            <td style="padding: 0px;height: 38px; width: 221px;">
                                                                    {{$assigned_room->class->subject->name}}
                                                                    @php 
                                                                        $assigned_end = $assigned_room->end_time; 
                                                                        $assigned_day = $assigned_room->day->name;
                                                                        $array_spans[$row_val][$col_val] = $assigned_room->row_span_num - 1;
                                                                        $assigned_flag = true;
                                                                        
                                                                    @endphp
                                                                    @for($j = 0; $j< $assigned_room->row_span_num; $j++)
                                                                        @php
                                                                            $tmp = $row_val;
                                                                            $array_compare[$tmp+$j][$col_val] = $assigned_room->row_span_num-$j;
                                                                        @endphp
                                                                    @endfor
                                                            </td>
                                                        @endif
                                                    @endforeach
                                                    @if ($row_val < 33 )
                                                        @if ($assigned_flag === false && $array_compare[$row_val][$col_val] === -1)
                                                            <td style="padding: 0px;height: 38px; width: 221px;">
                                                                <button type="button" data-room_seats={{$room->total_students}} data-time="{{$time->time}}" data-day="{{$day->name}}" data-day_id="{{$day->id}}" data-room_id="{{$room->id}}" data-room="{{$room->room_name}}"  class="btn btn-light" style="height: 51px; width: 221px;white-space: normal; font-size: 12px;border-radius:0px;" data-toggle="modal" data-target="#myModal" id="myButton">
                                                                </button>
                                                            </td>
                                                        @endif
                                                    @endif
                        
                                                    @php
                                                        $col_val++;
                                                    @endphp
                                                @endforeach
                                            </tr>
                                            @php
                                                $i++;
                                                $row_val++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div style="margin-top: 20px;">
                            <table id="new-customer" style="text-align: center;width: 100%; font-size: 15px; ">
                                <thead>
                                    <tr>
                                        <td style="width: 15%;border: none;">
                                            <label>1. Game Development Manager&ensp; 1</label><br>
                                            <label>1. Game Development Manager&ensp; 1</label><br>
                                            <label>1. Game Development Manager&ensp; 1</label><br>
                                            <label>1. Game Development Manager&ensp; 1</label><br>
                                            <label>1. Game Development Manager&ensp; 1</label><br>
                                        </td>
                                        <td class="th-lg" style="width: 15%; border: none; font-size: 12px;">
                                            <label style="padding-bottom: 5px;">បានឃើញ និងយល់ព្រម</label><br>
                                            <label>ជ.សាកលវិទ្យាធិការ</label><br>
                                            <label>សាកលវិទ្យាធិការរង</label>
                                        </td>
                                        <td class="th-lg" style="width: 15%; border: none; font-size: 12px;">
                                            <label>បានឃើញ​ និងពិនិត្យត្រឹមត្រូវ</label><br>
                                            <label>ព្រឹទ្ធបុរសមហាវិទ្យាល័យ. {{$department->name}}</label>
                                        </td>
                                        <td class="th-lg" style="width: 15%; border: none; font-size: 12px;">
                                            <label>បានឃើញ​ និងពិនិត្យត្រឹមត្រូវ</label><br>
                                            <label>ប្រធានការិយាល័យសិក្សា</label>
                                        </td>
                                        <td class="th-lg" style="width: 25%; border: none; font-size: 12px;">
                                            <label>ថ្ងៃព្រហស្បត្តិ៍​ ៩រោច​ ខែអាសាឍ ឆ្នាំកុរ ឯកស័ក ព.ស ២៥៦៣</label><br>
                                            <label>រាជធានីភ្នំពេញ ថ្ងៃទី២៥ ខែកក្កដា ឆ្នាំ២០១៩</label><br>
                                            <label>ប្រធានដេប៉ា. {{$department->name}}</label>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
</body>

</html>