<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\AssignedRoom;
use App\Building;
use App\Day;
use App\Department;
use App\DepartmentGroup;
use App\Faculty;
use App\Room;
use App\Time;
use Illuminate\Http\Request;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

// use niklasravnsborg\LaravelPdf\Pdf;

class DownloadScheduleController extends Controller
{
    /**
     * show preview of the schedule pdf
     * @param Request 
     * @param id
     */
    public function index(Request $request, $building_id){
        // dd($request);
        $building_id = $building_id;
        $room_id = $request->get('room_id');
        $department_id = $request->get('department_id');
        $group_id = $request->get('group_id');
        $subgroup_id = $request->get('subgroup_id');
        $faculty_id = $request->get('faculty_id');
        $semester = $request->get('semester');
        $time_shift = $request->get('time_shift');

        $days = Day::all();
        $assigned_rooms = AssignedRoom::all();
        $times = Time::where('time_shift_id', '=', $time_shift)->get();
        $building = Building::find($building_id);
        $department = Department::find($department_id);
        $group = DepartmentGroup::find($group_id);
        $faculty = Faculty::find($faculty_id);
        $room = Room::find($room_id);
        $assigned_rooms = AssignedRoom::where('department_id', '=', $department_id)->where('group_id', '=', $group_id)->get();

        // dd($faculty); 
        $pdf = Pdf::loadView('buildings.schedulePdfTemplate', [
            'assigned_rooms' => $assigned_rooms,
            'days' => $days,
            'times' => $times,
            'room' => $room, 
            'building_id' => $building_id,
            'building' => $building,
            'department' => $department,
            'faculty' => $faculty,
            'semester' => $semester,
            'group' => $group,
            ]);
            $pdf->SetProtection(['copy', 'print'], '', 'pass');
        return $pdf->stream('Schedule.pdf');
        // return view('buildings.testing', compact('assigned_rooms','days','times','room', 'building_id', 'building', 'department', 'faculty', 'semester', 'group'));
    }

    /**
     * direct download function for pdf
     */
    public function donwloadPdf($semester, $department_id, $group_id)
    {
        $pdf = PDF::loadView('buildings.testing');
            $pdf->SetProtection(['copy', 'print'], '', 'pass');
        return $pdf->stream('Schedule.pdf');

        # code...
        /**
         * semester I
         * Department
         * Group
         */
    }

    /**
     * testing function for generate pdf
     */
    public function testing()
    {
        # code...
        $title = "សាសា";
        $days = Day::all();
        $assigned_rooms = AssignedRoom::all();
        $times = Time::all();
        $pdf = PDF::loadView('buildings.schedulePdfTemplate',
        [
            'title' => $title,
            'assigned_rooms' => $assigned_rooms,
            'days' => $days,
            'times' => $times,
            'room' , 
            'building_id',
            'building' ,
            'department' ,
            'faculty',
            'semester',
            'group' ,
        ]        
        );
            $pdf->SetProtection(['copy', 'print'], '', 'pass');
        return $pdf->stream('Schedule.pdf');
    }
}
 