<?php

namespace App\Http\Controllers\V2;

use App\Classes;
use App\Day;
use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use App\Time;
use App\User;
use App\User_Dept;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfessorClassController extends Controller
{
    public function index(){
        $user_id = Auth::user()->id;
        $user_depts = DB::table('user_depts')->where('user_id', '=', $user_id)->get(); 
        // to get all user_dept along with user object where the user_id equal the login user
        // $user_depts = User_Dept::with('user')->where('user_id', '=', $user_id)->get(); 
        $classes = Classes::all();
        // dd($user_depts->toArray());
        // $classes = Classes::all();
        // $subjects = Subject::all();
        // $days = Day::all();
        // $times = Time::all();
        // $prof_depts = Prof_Dept::all();
        return view('classes.index',compact('classes', 'user_depts'));
    }

    public function create(){
        $departments = Department::all();
        $subjects = Subject::all();
        $days = Day::all();
        $times = Time::all();
        $prof_depts = User_Dept::with('user')->where('user_id', '=', Auth::user()->id)->get(); 
        // dd($days->toArray());

        return view('classes.create', compact('departments','subjects','days','times','prof_depts'));
    }

    public function store(request $request){
        // dd($request->all());
        list($startTime, $day) = explode("-", $request->get("time-day"), 2);
        $class_hours = $request->get('class_hours');
        switch ($class_hours) {
            case '1':{
                $storeEndTime = strtotime($startTime) + 3600;
                $resultEndTime = date('H:i:s', $storeEndTime);
                break;
            }
            case '1.5':{
                $storeEndTime = strtotime($startTime) + 5400;
                $resultEndTime = date('H:i:s', $storeEndTime);
                break;
            }
            case '2':{
                $storeEndTime = strtotime($startTime) + 7200;
                $resultEndTime = date('H:i:s', $storeEndTime);
                break;
            }
            case '2.5':{
                $storeEndTime = strtotime($startTime) + 9000;
                $resultEndTime = date('H:i:s', $storeEndTime);
                break;
            }
            case '3':{
                $storeEndTime = strtotime($startTime) + 10800;
                $resultEndTime = date('H:i:s', $storeEndTime);
                break;
            }
            default:
                $storeEndTime = strtotime($startTime);
                $resultEndTime = date('H:i:s', $storeEndTime);
                break;
        }
        $classes = new Classes([
            'subject_id' => $request->get('subject_id'),
            'user_dept_id' => $request->get('prof_dept_id'),
            'day' => $day,
            'start_time' => $startTime,
            'end_time' => $resultEndTime,
            'class_hours' => $class_hours,
        ]);
        $classes->save();
        return redirect('/ProfessorClasses')->with('success', 'Class has been added');
    }

    //     /**
    //      * Show the form for editing the specified resource.
    //      *
    //      * @param  int  $id
    //      * @return \Illuminate\Http\Response
    //      */
    public function edit($id)
    {
        $classes = Classes::find($id);
        $departments = Department::all();
        $subjects = Subject::all();
        $days = Day::all();
        $times = Time::all();
        $prof_depts = User_Dept::with('department')->where('user_id', '=', Auth::user()->id)->get();
        // dd($prof_depts->toArray());
        return view('classes.edit', compact('classes','departments','subjects','days','times','prof_depts'));
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function update(Request $request, $id)
    {
        // dd("in update");
        $classes = Classes::find($id);
        $classes->subject_id = $request->get('subject_id');
        $classes->user_dept_id = $request->get('prof_dept_id');
        $classes->day = $request->get('day_id');
        $classes->start_time = $request->get('start_time');
        $classes->end_time = $request->get('end_time');
        $classes->save();
        return redirect('/ProfessorClasses')->with('success', 'Update Classes');
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function destroy($id)
    {
        $classes = Classes::find($id);
        if( $classes->user_dept->user->id === Auth::user()->id){
            $classes->delete();
        } else {
            $this->authorize('create', User::class);
            $classes->delete();
        }
        return redirect('/ProfessorClasses')->with('success', 'Classes has been deleted Successfully');
    }
}
