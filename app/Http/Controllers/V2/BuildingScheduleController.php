<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\DisplayTime;
use Illuminate\Http\Request;
use App\Classes;
use App\Day;
use App\Time;
use App\Room;
use App\TimeShift;
use App\AssignedRoom;
use App\ClassRoom;
use Illuminate\Support\Facades\Validator;
use App\Building;
use App\Department;
use App\DepartmentGroup;
use App\Faculty;
use Illuminate\Support\Facades\DB;
 
class BuildingScheduleController extends Controller
{
    /**
     * function for showing the schedule of the building
     * @param id of building
     */
    public function index($id){

        $classes = Classes::all();
        $assigned_rooms = AssignedRoom::all();
        $days = Day::all();
        $times = Time::all();
        $rooms = Room::where('building_id', '=', $id)->get();
        $building = Building::find($id);
        $day_parts = TimeShift::all();
        $display_times = DisplayTime::all();
        $faculties = Faculty::all();
        $departments = Department::all();
        return view('buildings.schedule', compact('classes','assigned_rooms','days','times','rooms','day_parts', 'display_times', 'building', 'departments', 'faculties'));
    }

    /**
     * filter suitable class by 'start_time and total_students'
     * @param request
     */
    public function filterClasses(Request $request){

        $selectedTime = $request->get('room_time');
        $selectedClass = $request->get('room_seats');
        $classList = ClassRoom::where('start_time', '=', $selectedTime)->where('total_students','<=', $selectedClass)->get();

        $output = '<option value="">Select Subject</option>';
            foreach($classList as $class) {
                $output .= '<option value="'.$class->id.'">'.$class->subject->name.'</option>';
            }
        echo $output;
    }

    /**
     * to filter the department according to selected faculty
     * @param request
     */
    public function filteredFacultyDepartment(Request $request){
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $rows = Department::where('faculty_id', '=', $value)->get();
        
        $output = '<option value="">Select  ' .ucfirst($dependent).'</option>';
            foreach ($rows as $row){
                $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
            }
        echo $output;
    }

    /**
     * to filter the group according to department
     * @param request
     */
    public function filteredDepartmentGroup(Request $request){

        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $rows = DepartmentGroup::where('department_id', '=', $value)->get();
        
        $output = '<option value="">Select  ' .ucfirst($dependent).'</option>';
            foreach ($rows as $row){
                $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
            }
        echo $output;
    }

    /**
     * to filter out the selected day
     * @param request
     */
    public function filterDay(Request $request){
        echo 'hello';
        // $day = $request->get('day');
        // $days = Day::where('name', '!=' , $day)->orWhereNull('name')->get();
        // $output = '<option value="">Select  ' .day.'</option>';
        // foreach ($days as $day){
        //     $output .= '<option value="'.$day->id.'">'.$day->name.'</option>';
        // }
        // echo $output;
        // return response()->json($day);
    }
 
    /**
     * function for storing assign classes
     * @param request
     * @param id
     */
    public function store(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'room_id' => 'required',
            'class_id' => 'required',
            'day_id' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'department_id' => 'required',
            'group_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/schedules')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $explode_start_time  = explode(':', $request->start_time);
        $explode_end_time  = explode(':', $request->end_time);
        $start_min = ($explode_start_time[0] * 60.0 + $explode_start_time[1] * 1.0);
        $end_min = ($explode_end_time[0] * 60.0 + $explode_end_time[1] * 1.0);
        $row_span_num = ($end_min - $start_min)/30;

        $assigned_room = new AssignedRoom;
        $assigned_room->room_id = $request->room_id;
        $assigned_room->class_id = $request->class_id;
        $assigned_room->day_id = $request->day_id;
        $assigned_room->department_id = $request->department_id;
        $assigned_room->group_id = $request->group_id;
        $assigned_room->row_span_num = $row_span_num;
        $assigned_room->start_time = $request->start_time;
        $assigned_room->end_time = $request->end_time;
        $assigned_room->save();

        $willDeleteId = $request->class_id;

        ClassRoom::find($willDeleteId)->delete();

        return redirect()->action('BuildingScheduleController@index', ['id' => $id])->with('message', 'Class has been assigned Successfully!!! ');
    }

    /**
     * test function for building contain half hour classes
     * @param building_id
     */
    public function test($building_id)
    {
        $classes = Classes::all();
        $assigned_rooms = AssignedRoom::all();
        $days = Day::all();
        $times = Time::all();
        $faculties = Faculty::all();
        $building = Building::find($building_id); 
        $day_parts = TimeShift::all();
        $display_times = DisplayTime::all();
        $display_evenId_times = DB::table('display_times')->whereRaw('MOD(id, 2) = 0')->get();
        $display_oddId_times = DB::table('display_times')->whereRaw('MOD(id, 2) != 0')->get();
        // dd($display_oddId_times);
        // $rooms = Room::where("building_id", '=', $building_id)
        // ->offset(10)
        // ->limit(1)
        // ->get();
        $rooms = Room::all();
        $faculties = Faculty::all();
        $departments = Department::all();
        // dd($assigned_rooms);
        return view('buildings.testing_30Schedule', compact('classes','assigned_rooms','days','times','rooms','day_parts', 'display_times', 'display_evenId_times', 'display_oddId_times', 'building', 'departments', 'faculties'));
    }
}
