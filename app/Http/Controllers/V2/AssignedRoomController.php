<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AssignedRoom;
use App\Classes;
use App\Day;
use App\Time;
use App\Room;
use App\TimeShift;
use Illuminate\Support\Facades\Validator;
use App\ClassRoom;
use App\DisplayTime;

class AssignedRoomController extends Controller
{

    /**
     *
     */
    public function index(){
        $classes = Classes::all();
        $assigned_rooms = AssignedRoom::all();
        $days = Day::all();
        $times = Time::all();
        $rooms = Room::all();
        $day_parts = TimeShift::all();
        $test_times = DisplayTime::all();
        return view('schedule.index', compact('classes','assigned_rooms','days','times','rooms','day_parts', 'test_times'));
    }



}
