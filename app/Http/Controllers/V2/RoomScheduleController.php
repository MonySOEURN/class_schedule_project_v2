<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\day;
use App\Time;
use App\room;
use App\AssignedRoom;
use App\Building;
use App\ClassRoom;
use App\Department;
use App\DepartmentGroup;
use Illuminate\Support\Facades\Validator;

class RoomScheduleController extends Controller
{
    public function index($building_id, $room_id){
        // dd($building_id.' '.$room_id );
        $days = day::all();
        $times = Time::all();
        $room = room::find($room_id);
        $rooms = Room::where('building_id', '=', $building_id)->get();
        $assigned_rooms = AssignedRoom::all();
        $departments = Department::all();
        return view('rooms.schedule',compact('days', 'times', 'room', 'rooms', 'assigned_rooms', 'building_id', 'departments'));
    }

    public function filterClasses(Request $request){

        $selectedTime = $request->get('room_time');
        $selectedClass = $request->get('room_seats');

        $classList = ClassRoom::where('start_time', '=', $selectedTime)->where('total_students','<=', $selectedClass)->get();

        $output = '<option value="">Select Subject</option>';
        foreach($classList as $class)
        {
            $output .= '<option value="'.$class->id.'">'.$class->subject->name.'</option>';
        }
        echo $output;

        // return view('shedule.index', compact('classed'));
    }


    /**
     * to filter the group according to department
     * @param request
     */
    public function filteredDepartmentGroup(Request $request){

        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $rows = DepartmentGroup::where('department_id', '=', $value)->get();
        
        $output = '<option value="">Select  ' .ucfirst($dependent).'</option>';
            foreach ($rows as $row){
                $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
            }
        echo $output;
    }

    public function store(Request $request, $room_id, $building_id){
//         dd($request);
        $validator = Validator::make($request->all(), [
            'room_id' => 'required',
            'class_id' => 'required',
            'day_id' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/schedules')
                        ->withErrors($validator)
                        ->withInput();
        }


        $explode_start_time  = explode(':', $request->start_time);
        $explode_end_time  = explode(':', $request->end_time);
        $start_min = ($explode_start_time[0] * 60.0 + $explode_start_time[1] * 1.0);
        $end_min = ($explode_end_time[0] * 60.0 + $explode_end_time[1] * 1.0);
        $row_span_num = ($end_min - $start_min)/30;

        $assigned_room = new AssignedRoom;
        $assigned_room->room_id = $room_id;
        $assigned_room->class_id = $request->class_id;
        $assigned_room->day_id = $request->day_id;
        $assigned_room->department_id = $request->department_id;
        $assigned_room->group_id = $request->group_id;
        $assigned_room->row_span_num = $row_span_num;
        $assigned_room->start_time = $request->start_time;
        $assigned_room->end_time = $request->end_time;
        $assigned_room->save();

        return redirect()->action('RoomScheduleController@index', ['building_id'=> $building_id,'room_id' => $room_id])->with('message', 'Class has been assigned Successfully!!! ');
    }

    public function test($building_id, $room_id){
        // dd($building_id.' '.$room_id );
        $days = day::all();
        $times = Time::all();
        $room = room::find($room_id);
        $rooms = Room::where('building_id', '=', $building_id)->get();
        $assigned_rooms = AssignedRoom::all();
        return view('rooms.testing_30Schedule',compact('days', 'times', 'room', 'rooms', 'assigned_rooms', 'building_id'));
    }
}
