<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\AssignedRoom;
use App\Building;
use App\Classes;
use App\Day;
use App\DisplayTime;
use App\Floor;
use App\Room;
use App\Time;
use App\TimeShift;
use App\UsedRooms;
use Illuminate\Http\Request;

class UsedRoomController extends Controller
{
    public function index(){

        $buildings = Building::all();
        $rooms = Room::where('used_room', '=', 1)->get();
        return view('used_rooms.index')
        ->with('buildings', $buildings)
        ->with('rooms', $rooms);
    }

    public function filterAvailableRooms(Request $request){
        $building_id = $request->get('building_id');
        $rows = Room::where('building_id', '=', $building_id)->where('used_room', '=', 0)->get();
        
        $output = '<option value="">Choose Rooms</option>';
            foreach ($rows as $row){
                $output .= '<option value="'.$row->id.'" onclick="test()">'.$row->name.'</option>';
            }
        echo $output;
    }

    public function update($building_id , $room_id){
        
        $building = Building::find($building_id);
        $room = Room::find($room_id);
        $floors = Floor::all();
        $room->used_room = 1;
        $room->save();

        return redirect(route('buildings.show', $building_id))
        ->with('building', $building)
        ->with('floors',$floors);
    }

    public function updateRoom(Request $request){
        dd($request);
    }

    public function delete($building_id , $room_id){
        
        $room = Room::find($room_id);
        $room->used_room = 0;
        $room->save();

        $buildings = Building::all();
        $rooms = Room::where('used_room', '=', 1)->get();

        return view('used_rooms.index')
        ->with('buildings', $buildings)
        ->with('rooms', $rooms);
    }
}
