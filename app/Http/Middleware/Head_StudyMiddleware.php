<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Head_StudyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role === 'head_department' || 
        Auth::user()->role === 'study_office' || 
        Auth::user()->role === 'foundation_office' || 
        Auth::user()->role === 'admin' ){
            return $next($request);
        }
        return abort(403, 'Unauthorized action.');//response('not allowed to enter the route');
    }
}
