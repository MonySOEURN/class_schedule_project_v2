<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeShift extends Model
{
    protected $table = 'time_shifts';
}
