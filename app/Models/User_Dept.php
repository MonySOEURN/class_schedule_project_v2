<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Dept extends Model
{
    protected $table = 'user_depts';

    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
