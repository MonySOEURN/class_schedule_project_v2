<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name',
        'faculty_id'
    ];

    public function user()
    {
        return $this->belongsToMany('App\User', 'user_dept', 'department_id', 'user_id');
    }

}
