<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $fillable = [
        'name'
    ];

    public function departments()
    {
        return $this->belongsToMany('App\Department', 'user_dept', 'user_id', 'department_id');
    }
}
