<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisplayTime extends Model
{
    protected $table = 'display_times';
}
