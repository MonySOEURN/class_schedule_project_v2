<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsedRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('used_rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('scheduled_id')->unsigned();
            $table->bigInteger('used_room_detail_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('scheduled_id')->references('id')->on('scheduleds')->onDelete('cascade');
            $table->foreign('used_room_detail_id')->references('id')->on('used_room_details')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('used_rooms');
    }
}
