<?php

use Illuminate\Database\Seeder;

class DepartmentGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\DepartmentGroup::insert(array(

            array('name' => 'B', 'department_id' => '1', 'year_num' => '1'),
            array('name' => 'A', 'department_id' => '1', 'year_num' => '2'),
            array('name' => 'C', 'department_id' => '1', 'year_num' => '3'),

            array('name' => 'D', 'department_id' => '2', 'year_num' => '1'),
            array('name' => 'E', 'department_id' => '2', 'year_num' => '2'),
            array('name' => 'F', 'department_id' => '2', 'year_num' => '3'),


            array('name' => 'G', 'department_id' => '3', 'year_num' => '1'),
            array('name' => 'H', 'department_id' => '3', 'year_num' => '2'),
            array('name' => 'I', 'department_id' => '3', 'year_num' => '3'),

            array('name' => 'J', 'department_id' => '4', 'year_num' => '1'),
            array('name' => 'K', 'department_id' => '4', 'year_num' => '2'),
            array('name' => 'L', 'department_id' => '4', 'year_num' => '3'),

            array('name' => 'M', 'department_id' => '5', 'year_num' => '1'),
            array('name' => 'N', 'department_id' => '5', 'year_num' => '2'),
            array('name' => 'O', 'department_id' => '5', 'year_num' => '3'),
            )
        );
    }
}
