<?php

use Illuminate\Database\Seeder;

class AssignedRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assigned_rooms')->insert([

            array('room_id' => '11', 'class_id' => '1','day_id' => '1','department_id' => '1', 'group_id' => '1', 'row_span_num' => '3', 'start_time' => '7:00:00', 'end_time' => '8:30:00'),
            array('room_id' => '11', 'class_id' => '2','day_id' => '1','department_id' => '1', 'group_id' => '1', 'row_span_num' => '5', 'start_time' => '8:30:00', 'end_time' => '10:30:00'),

            array('room_id' => '11', 'class_id' => '3','day_id' => '1','department_id' => '1', 'group_id' => '1', 'row_span_num' => '2', 'start_time' => '13:00:00', 'end_time' => '14:00:00'),

            array('room_id' => '11', 'class_id' => '4','day_id' => '1','department_id' => '1', 'group_id' => '1', 'row_span_num' => '5', 'start_time' => '18:00:00', 'end_time' => '20:30:00'),
            
        ]);
    }
}
