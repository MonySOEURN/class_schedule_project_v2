<?php

use Illuminate\Database\Seeder;

class UserDeptsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User_Dept::insert(array(

            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),

            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            array('user_id' => rand(1, 5), 'department_id' => rand(1, 26)),
            
        ));
    }
}
