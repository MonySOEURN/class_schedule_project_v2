<?php

use Illuminate\Database\Seeder;

class TimeShiftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('time_shifts')->insert([
            array('name' => 'Morning'),
            array('name' => 'Noon'),
            array('name' => 'Evening'),
        ]);
    }
}
