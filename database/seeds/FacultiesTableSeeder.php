<?php

use Illuminate\Database\Seeder;

class FacultiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Faculty::insert(array(
            array('name'=>'Faculty of Science'),
            array('name'=>'Faculty of Social Science and Humanities'),
            array('name'=>'Faculty of Engineering'),
            array('name'=>'Faculty of Development Studies'),
            array('name'=>'Faculty of Education')
        ));

    }
}
