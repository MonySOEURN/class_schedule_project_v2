<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Subject::insert([
            'name'=>"General English III"
        ]);
        \App\Subject::insert([
            'name'=>"Advance Programming I"
        ]);
        \App\Subject::insert([
            'name'=>"Data Structure and Algorithm I	"
        ]);
        \App\Subject::insert([
            'name'=>"Data Communication I"
        ]);
        \App\Subject::insert([
            'name'=>"Mathematic III"
        ]);
        \App\Subject::insert([
            'name'=>"Probability and Statistics"
        ]);
        \App\Subject::insert([
            'name'=>"Circuit Theory I"
        ]);
        \App\Subject::insert([
            'name'=>"Critical Thinking & Personal Development"
        ]);
        \App\Subject::insert([
            'name'=>"Project Practicum"
        ]);
        \App\Subject::insert([
            'name'=>"Discrete Math"
        ]);
        \App\Subject::insert([
            'name'=>"Signal & System"
        ]);
        \App\Subject::insert([
            'name'=>"Database II"
        ]);
        \App\Subject::insert([
            'name'=>"User Interface Design"
        ]);
        \App\Subject::insert([
            'name'=>"Network Engineering I"
        ]);
        \App\Subject::insert([
            'name'=>"Computer Graphic I"
        ]);
        \App\Subject::insert([
            'name'=>"Object Oriented Analysis and Design"
        ]);
        \App\Subject::insert([
            'name'=>"Operating System"
        ]);
        \App\Subject::insert([
            'name'=>"Mobile Application Development I"
        ]);
    }
}
