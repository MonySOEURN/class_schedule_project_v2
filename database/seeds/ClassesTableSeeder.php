<?php

use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Classes::insert(array(
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Monday', 'start_time' => '07:00:00', 'end_time' => '08:00:00', 'class_hours' => '1'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Tuesday', 'start_time' => '08:00:00', 'end_time' => '08:00:00', 'class_hours' => '1.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Wednesday', 'start_time' => '09:00:00', 'end_time' => '08:00:00', 'class_hours' => '2'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Thursday', 'start_time' => '10:00:00', 'end_time' => '08:00:00', 'class_hours' => '2.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Friday', 'start_time' => '11:00:00', 'end_time' => '08:00:00', 'class_hours' => '3'),

            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Saturday', 'start_time' => '12:00:00', 'end_time' => '08:00:00', 'class_hours' => '1'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Monday', 'start_time' => '13:00:00', 'end_time' => '08:00:00', 'class_hours' => '1.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Tuesday', 'start_time' => '14:00:00', 'end_time' => '08:00:00', 'class_hours' => '2'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Wednesday', 'start_time' => '15:00:00', 'end_time' => '08:00:00', 'class_hours' => '2.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Thursday', 'start_time' => '16:00:00', 'end_time' => '08:00:00', 'class_hours' => '3'),

            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Friday', 'start_time' => '17:00:00', 'end_time' => '08:00:00', 'class_hours' => '1'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Saturday', 'start_time' => '18:00:00', 'end_time' => '08:00:00', 'class_hours' => '1.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Monday', 'start_time' => '19:00:00', 'end_time' => '08:00:00', 'class_hours' => '2'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Tuesday', 'start_time' => '20:00:00', 'end_time' => '08:00:00', 'class_hours' => '2.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Wednesday', 'start_time' => '21:00:00', 'end_time' => '08:00:00', 'class_hours' => '3'),

            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Thursday', 'start_time' => '22:00:00', 'end_time' => '08:00:00', 'class_hours' => '1'),

        ));

        \App\Classes::insert(array(
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Friday', 'start_time' => '07:30:00', 'end_time' => '08:00:00', 'class_hours' => '1'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Saturday', 'start_time' => '08:30:00', 'end_time' => '08:00:00', 'class_hours' => '1.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Monday', 'start_time' => '09:30:00', 'end_time' => '08:00:00', 'class_hours' => '2'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Tuesday', 'start_time' => '10:30:00', 'end_time' => '08:00:00', 'class_hours' => '2.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Wednesday', 'start_time' => '11:30:00', 'end_time' => '08:00:00', 'class_hours' => '3'),

            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Thursday', 'start_time' => '12:30:00', 'end_time' => '08:00:00', 'class_hours' => '1'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Friday', 'start_time' => '13:30:00', 'end_time' => '08:00:00', 'class_hours' => '1.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Saturday', 'start_time' => '14:30:00', 'end_time' => '08:00:00', 'class_hours' => '2'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Monday', 'start_time' => '15:30:00', 'end_time' => '08:00:00', 'class_hours' => '2.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Tuesday', 'start_time' => '16:30:00', 'end_time' => '08:00:00', 'class_hours' => '3'),

            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Wednesday', 'start_time' => '17:30:00', 'end_time' => '08:00:00', 'class_hours' => '1'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Thursday', 'start_time' => '18:30:00', 'end_time' => '08:00:00', 'class_hours' => '1.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Friday', 'start_time' => '19:30:00', 'end_time' => '08:00:00', 'class_hours' => '2'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Saturday', 'start_time' => '20:30:00', 'end_time' => '08:00:00', 'class_hours' => '2.5'),
            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Monday', 'start_time' => '21:30:00', 'end_time' => '08:00:00', 'class_hours' => '3'),

            array('subject_id' => rand(1,18), 'user_dept_id' => rand(1, 15), 'day' => 'Tuesday', 'start_time' => '22:30:00', 'end_time' => '08:00:00', 'class_hours' => '1'),

        ));
    }
}
