<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::insert(array(

            array('role' => 'professor', 'name' => 'testing_professor', 'email' => 'test.professor@testing.com', 'password'=> bcrypt('12345678')),
            array('role' => 'head_department', 'name' => 'testing_head_dept', 'email' => 'test.head_dept@testing.com', 'password'=> bcrypt('12345678')),
            array('role' => 'study_office', 'name' => 'testing_study_office', 'email' => 'test.study_office@testing.com', 'password'=> bcrypt('12345678')),
            array('role' => 'foundation_office', 'name' => 'testing_foundation_office', 'email' => 'test.foundation_office@testing.com', 'password'=> bcrypt('12345678')),
            array('role' => 'admin', 'name' => 'testing_admin', 'email' => 'test.admin@testing.com', 'password'=> bcrypt('12345678')),

        ));
    }
}
