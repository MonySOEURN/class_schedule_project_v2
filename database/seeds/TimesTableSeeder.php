<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('times')->insert([
            'time' => '07:00', 
            'time_shift_id' => 1
        ]);
        DB::table('times')->insert([
            'time' => '07:30', 
            'time_shift_id' => 1
        ]);
        DB::table('times')->insert([
            'time' => '08:00',
            'time_shift_id' => 1,

        ]);

        DB::table('times')->insert([
            'time' => '08:30',
            'time_shift_id' => 1,

        ]);
        DB::table('times')->insert([
            'time' => '09:00',
            'time_shift_id' => 1,
        ]);
        DB::table('times')->insert([
            'time' => '09:30',
            'time_shift_id' => 1,
        ]);
        DB::table('times')->insert([
            'time' => '10:00',
            'time_shift_id' => 1,
        ]);
        DB::table('times')->insert([
            'time' => '10:30',
            'time_shift_id' => 1,
        ]);
        DB::table('times')->insert([
            'time' => '11:00',
            'time_shift_id' => 1,
        ]);
        DB::table('times')->insert([
            'time' => '11:30',
            'time_shift_id' => 1,
        ]);
        DB::table('times')->insert([
            'time' => '12:00',
            'time_shift_id' => 1,
        ]);
        DB::table('times')->insert([
            'time' => '12:30',
            'time_shift_id' => 1,
        ]);
        DB::table('times')->insert([
            'time' => '13:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '13:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '14:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '14:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '15:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '15:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '16:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '16:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '17:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '17:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '18:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '18:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '19:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '19:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '20:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '20:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '21:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '21:30',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '22:00',
            'time_shift_id' => 2,
        ]);
        DB::table('times')->insert([
            'time' => '22:30',
            'time_shift_id' => 2,
        ]);
    }
}
