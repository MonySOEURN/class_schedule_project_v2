<?php

use Illuminate\Database\Seeder;

class UsedRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\UsedRooms::insert(array(
            //for room that need to be use
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),
            array('room_id'=> rand(1,149)),


        ));
    }
}
