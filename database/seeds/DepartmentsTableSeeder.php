<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Department::insert(array(
            //Faculty of Science
            array('name'=>'Biology','faculty_id'=>1),
            array('name'=>'Chemistry','faculty_id'=>1),
            array('name'=>'Computer Science','faculty_id'=>1),
            array('name'=>'Environmental Science','faculty_id'=>1),
            array('name'=>'Mathematics','faculty_id'=>1),
            array('name'=>'Physics','faculty_id'=>1),

            //Faculty of Social Science and Humanities
            array('name'=>'Geography','faculty_id'=>2),
            array('name'=>'History','faculty_id'=>2),
            array('name'=>'Khmer Literature','faculty_id'=>2),
            array('name'=>'Linguistics','faculty_id'=>2),
            array('name'=>'Media and Communication','faculty_id'=>2),
            array('name'=>'Philosophy','faculty_id'=>2),
            array('name'=>'Psychology','faculty_id'=>2),
            array('name'=>'Sociology','faculty_id'=>2),
            array('name'=>'Social Work','faculty_id'=>2),
            array('name'=>'Tourism','faculty_id'=>2),

            //Faculty of Engineering
            array('name'=>'Information Technology Engineering','faculty_id'=>3),
            array('name'=>'Telecommunication and Electronic Engineering','faculty_id'=>3),
            array('name'=>'Bioengineering','faculty_id'=>3),

            //Faculty of Development Studies
            array('name'=>'Community Development','faculty_id'=>4),
            array('name'=>'Economic Development','faculty_id'=>4),
            array('name'=>'Natural Resource Management and Development','faculty_id'=>4),

            //Faculty of Education
            array('name'=>'Educational Studies','faculty_id'=>5),
            array('name'=>'Higher Education Development and Management','faculty_id'=>5),
            array('name'=>'Lifelong Learning','faculty_id'=>5),
            array('name'=>'Center for Educational Research and Training','faculty_id'=>5)
        ));
    }
}
